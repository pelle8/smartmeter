## Fronuis Smart Meter TS5KA-3 Registers 

Polled at startup, haven't reversed those yet

```
REG_0: [2386, 0]
REG_11: [733]
REG_770: [1, 5]
REG_20480: [49, 52, 52, 53, 48, 53, 87, 40335, 1449] S/N
REG_20496: [2022]
REG_4098: [0]
REG_4355: [1]
REG_4356: [1, 0]
```

Apart from above, the other registers look the same as TS65A-3
