Looking for BYD registers, scripts (passive modbus rtu listener, modbus tcp client, etc), wiring of Tesla
M3 battery....head over to:
https://gitlab.com/pelle8/inverter_resources

If you are looking for the RPi code to hook up a battery to a Gen24, head over to: https://gitlab.com/pelle8/batteryemulator

# Modbus registers

[Fronius SmartMeter](./fronius_smartmeter_registers.md)

[Sungrow - DTSU666](./dtsu_smartmeter_registers.md)

# Distribute power over inverters or just extending a smart meter over IP using RPi(s)

This is how it looks like in Gen24 GUI:
![Gen24 meter extender](./fake_fronius_smart_meter.png "Gen24 meter extender")

The code is over [here](./code/README.md)
