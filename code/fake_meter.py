#!/usr/bin/env python
"""
Pymodbus Server With Updating Thread
- Modified to spoof a Smart Meter // Per Carlen
- Supported meters: Sungrow DTSU666, Fronius TS65A-3
"""
import asyncio
import binascii
import copy
import json
import math
import os
import re
import sys
import syslog
import time

import paho.mqtt.client as paho
import yaml
# --------------------------------------------------------------------------- #
# import the modbus libraries we need
# --------------------------------------------------------------------------- #
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext, ModbusSparseDataBlock
from pymodbus.device import ModbusDeviceIdentification
from pymodbus.server import StartAsyncSerialServer, StartAsyncTcpServer
from pymodbus.transaction import ModbusRtuFramer

# FORMAT = ('%(asctime)-15s %(threadName)-15s'
#          ' %(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
# logging.basicConfig(format=FORMAT)
# log = logging.getLogger()
# log.setLevel(logging.DEBUG)

# --------------------------------------------------------------------------- #
# some global vars
# --------------------------------------------------------------------------- #
meter_debug = 0

meter_timeout = 120
last_updated = 0

def read_config_file():
    config_json = ""
    filename = "fake_meter.yaml"
    print("reading from:", filename)
    try:
        path = "/etc/smartmeter/"
        if os.path.isfile(path + filename):
            filename = path + filename
        with open(filename, 'r') as file:
            config_json = json.dumps(yaml.safe_load(file))
    except:
        print("ERROR: No config file - ", filename)
        exit(1)

    return config_json


# --------------------------------------------------------------------------- #
# configure logging
# --------------------------------------------------------------------------- #
import logging
if meter_debug:
    logging.basicConfig()
    log = logging.getLogger()
    log.setLevel(logging.DEBUG)


def init_datastore(inverter_interface_type, fake_unique_id,meter_address):
    # ----------------------------------------------------------------------- # 
    # initialize your data store
    # ----------------------------------------------------------------------- # 
    slave_id = 0
    context = False

    if re.search(r'Fronius_ts65a3', inverter_interface_type):
        print("Init Fronius_ts65a3 datastore")
        p0 = [2336, 0]
        p11 = [731]
        p770 = [1, 3]
        p4098 = [1]
        p4355 = [1]
        p4356 = [1, 0]
        p20480 = [50, 52, 48, 50, 54, 54, 87, 31974, (1451 + int(fake_unique_id) - 1)]
        p20496 = [2022]
        block = ModbusSparseDataBlock(
            {1: p0, 12: p11, 771: [1, 3], 20481: p20480, 20497: p20496, 4099: p4098, 4356: p4355, 4357: p4356,
             258: [0] * 17, 286: [0] * 43, 1024: [0] * 17, 768: [0] * 2})
        store = ModbusSlaveContext(di=block, co=block, hr=block, ir=block, unit=meter_address)
        context = ModbusServerContext(slaves=store, single=True)

    if re.search(r'Fronius_ts5ka3', inverter_interface_type): 
        print("Init Fronius_ts5ka3 datastore")
        p0 = [2386, 0]
        p11 = [731]
        p770 = [1, 3]
        p4098 = [1]
        p4355 = [1]
        p4356 = [1, 0]
        p20480 = [49, 52, 52, 53, 48, 53, 87, 40335, (1451 + int(fake_unique_id) - 1)]
        p20496 = [2022]
        block = ModbusSparseDataBlock(
            {1: p0, 12: p11, 771: [1, 3], 20481: p20480, 20497: p20496, 4099: p4098, 4356: p4355, 4357: p4356,
             258: [0] * 17, 286: [0] * 43, 1024: [0] * 17, 768: [0] * 2})
        store = ModbusSlaveContext(di=block, co=block, hr=block, ir=block, unit=meter_address)
        context = ModbusServerContext(slaves=store, single=True)
        
    if re.search(r'Sungrow_dtsu666', inverter_interface_type): 
        print("Init Sungrow_dtsu666 datastore")
        p63 = [16128]
        p119 = [4999]
        p356 = [0, 269, 65535, 65494, 0, 29, 0, 256, 0, 0, 0, 0, 0, 0, 0, 0]
        p20480 = [8405]
        p97 = [2300, 2308, 2308, 180, 180, 97]
        p10 = [0, 11915, 0, 11915, 0, 0, 0, 0, 0, 0, 0, 3209]

        block = ModbusSparseDataBlock({11: p10, 64: p63, 98: p97, 120: p119, 357: p356, 20481: p20480})
        store = ModbusSlaveContext(di=block, co=block, hr=block, ir=block, unit=meter_address)
        context = ModbusServerContext(slaves=store, single=True)

    return context


def mqtt_on_message(mqtt_client, userdata, message, tmp=None):
    global meter_debug
    global last_updated
    global meter_json_data
    global meter_json_data_previous
    global context

    if meter_debug:
        print(" Received message " + str(message.payload)
              + " on topic '" + message.topic
              + "' with QoS " + str(message.qos))

    try:
        json_data = json.loads(message.payload)
        if type(json_data) is dict or type(json_data) is list:
            last_updated = json_data['last_updated']
            meter_json_data_previous = copy.deepcopy(meter_json_data)
            meter_json_data = json_data
            check_registers(context)
    except Exception as err:
        print(f"Unexpected {err=}, {type(err)=}")
        pass


def mqtt_on_subscribe(client, userdata, mid, granted_qos):
    pass


def mqtt_on_connect(client, userdata, flags, rc):
    global fake_topic

    if rc != 0:
        print("MQTT connection problem")
        client.connected_flag = False
    else:
        print("MQTT client connected:" + str(client))
        client.connected_flag = True
        client.subscribe(fake_topic, 2)


def get_32bits_from_value(value) -> list:
    value = int(value)
    if value >= 0:
        r1 = value & 65535
        r2 = value >> 16
    else:
        value = - value
        r1 = 65535 - (value & 65535)
        r2 = 65535 - (value >> 16)
    return [r1, r2]


def compare_lists(list1, list2, debug=False) -> bool:
    if len(list1) != len(list2):
        return False
    return list1 == list2


def compare_dicts(hash1,hash2,debug=False) -> bool:
    return hash1 == hash2

# --------------------------------------------------------------------------- #
# check if meter_data is actually changing, this will fail if real_meter is
# sending same data or if it has stopped sending
# --------------------------------------------------------------------------- #

async def check_meter_data():
    global meter_timeout
    global modbus_data
    global modbus_data_previous
    global modbus_same_data_counter

    while True:
        # need to exit ugly, if we want to do backup power
        if 'last_updated' in meter_json_data:
            last_updated = meter_json_data['last_updated']
            timestamp_now = int(time.time())
            if meter_debug: print("Checking if data is old")
            if (timestamp_now - int(last_updated) > int(meter_timeout)):
                syslog.syslog(syslog.LOG_ERR, "ERROR - Timeout on new data from real meter, exiting")
                print("Timeout on new data from real meter, exiting")
                exit(1)

        if modbus_same_data_counter > 6:
            syslog.syslog(syslog.LOG_ERR, "ERROR - Same modbus data now for a while modbus_data: ", modbus_data,
                          "modbus_data_previous:", modbus_data_previous)
            print("ERROR! Same modbus data now for a while, I'm going to quit!")
            exit(1)

        await asyncio.sleep(5)


def check_modbus_data_values(modbus_data,modbus_data_previous,modbus_same_data_counter) -> int:

    if compare_lists(modbus_data,modbus_data_previous):
        modbus_same_data_counter += 1
        if meter_debug: print("Comparing current modbus data with previous - same data now - identical ",
                              modbus_same_data_counter, "times")
    else:
        modbus_same_data_counter = 0
        if meter_debug: print("Comparing current modbus data with previous - same data now ",
                              modbus_same_data_counter, "times")
    return modbus_same_data_counter


def convert_values_to_register(meter_json_data,inverter_interface_type):
    global meter_debug
    global meter_same_data_counter
    global modbus_same_data_counter
    global modbus_data

    registers = {}
    e_consumed = meter_json_data['e_consumed']
    e_produced = meter_json_data['e_produced']
    if meter_json_data.get('er_consumed') is not None:
        er_consumed = meter_json_data['er_consumed']
    else:
        er_consumed = 0
    if meter_json_data.get('er_produced') is not None:
        er_produced = meter_json_data['er_produced']
    else:
        er_produced = 0
    frequency = meter_json_data['f']
    u1 = meter_json_data['u1']
    u2 = meter_json_data['u2']
    u3 = meter_json_data['u3']
    u_mean = (u1 + u2 + u3) / 3
    u_mean_phases = u_mean * 1.73  # approx
    i1 = meter_json_data['i1']
    i2 = meter_json_data['i2']
    i3 = meter_json_data['i3']

    # only sungrow?
    if i1 < 0: i1 = -i1
    if i2 < 0: i2 = -i2
    if i3 < 0: i3 = -i3

    p1 = meter_json_data['p1']
    p2 = meter_json_data['p2']
    p3 = meter_json_data['p3']
    ptot = meter_json_data['pt']

    pr1 = meter_json_data['pr1']
    pr2 = meter_json_data['pr2']
    pr3 = meter_json_data['pr3']
    prtot = meter_json_data['prt']

    pa1 = meter_json_data['pa1']
    pa2 = meter_json_data['pa2']
    pa3 = meter_json_data['pa3']
    patot = meter_json_data['pat']

    pf1 = meter_json_data['pf1']
    pf2 = meter_json_data['pf2']
    pf3 = meter_json_data['pf3']
    pftot = meter_json_data['pft']

    if meter_debug: print("-------------------------", time.time(), "modbus same counter: ", modbus_same_data_counter)
    if meter_debug: print("Type:", inverter_interface_type)
    if meter_debug: print("p1:", p1, "p2:", p2, "p3:", p3, "ptot:", ptot)
    if meter_debug: print("pr1:", pr1, "pr2:", pr2, "pr3:", pr3, "prtot:", prtot)
    if meter_debug: print("pa1:", pa1, "pa2:", pa2, "pa3:", pa3, "patot:", patot)
    if meter_debug: print("pf1:", pf1, "pf2:", pf2, "pf3:", pf3, "pftot:", pftot)
    if meter_debug: print("i1:", i1, "i2:", i2, "i3:", i3)
    if meter_debug: print("u1:", u1, "u2:", u2, "u3:", u3)
    if meter_debug: print("modbus:", modbus_data)
    if meter_debug: print("previe:", modbus_data_previous)

    if 'Fronius' in inverter_interface_type:
        reg_1024 = \
            get_32bits_from_value(math.trunc(e_consumed)) + \
            get_32bits_from_value((e_consumed - math.trunc(e_consumed)) * 1000) + \
            get_32bits_from_value(math.trunc(er_consumed)) + \
            get_32bits_from_value((er_consumed - math.trunc(er_consumed)) * 1000) + \
            get_32bits_from_value(math.trunc(e_produced)) + \
            get_32bits_from_value((e_produced - math.trunc(e_produced)) * 1000) + \
            get_32bits_from_value(math.trunc(er_produced)) + \
            get_32bits_from_value((er_produced - math.trunc(er_produced)) * 1000)

        reg_258 = \
            get_32bits_from_value(u_mean * 10) + \
            get_32bits_from_value(u_mean_phases * 10) + \
            get_32bits_from_value(ptot * 10) + \
            get_32bits_from_value(patot * 10) + \
            get_32bits_from_value(prtot * 10) + \
            get_32bits_from_value(round(pftot * 10 * 100)) + \
            get_32bits_from_value(0) + \
            get_32bits_from_value(frequency * 10)

        reg_286 = \
            get_32bits_from_value(u1 * 1.73 * 10) + \
            get_32bits_from_value(u1 * 10) + \
            get_32bits_from_value(i1 * 1000) + \
            get_32bits_from_value(p1 * 10) + \
            get_32bits_from_value(pa1 * 10) + \
            get_32bits_from_value(pr1 * 10) + \
            get_32bits_from_value(round(pf1 * 10 * 100)) + \
            get_32bits_from_value(u2 * 1.73 * 10) + \
            get_32bits_from_value(u2 * 10) + \
            get_32bits_from_value(i2 * 1000) + \
            get_32bits_from_value(p2 * 10) + \
            get_32bits_from_value(pa2 * 10) + \
            get_32bits_from_value(pr2 * 10) + \
            get_32bits_from_value(round(pf2 * 10 * 100)) + \
            get_32bits_from_value(u3 * 1.73 * 10) + \
            get_32bits_from_value(u3 * 10) + \
            get_32bits_from_value(i3 * 1000) + \
            get_32bits_from_value(p3 * 10) + \
            get_32bits_from_value(pa3 * 10) + \
            get_32bits_from_value(pr3 * 10) + \
            get_32bits_from_value(round(pf3 * 10 * 100))
        registers = {"reg_258": reg_258,"reg_286": reg_286,"reg_1024": reg_1024}

    if 'Sungrow_dtsu666' in inverter_interface_type:
        reg_356 = encode_value_to_32bits(p1) + \
            encode_value_to_32bits(p2) + \
            encode_value_to_32bits(p3) + \
            encode_value_to_32bits(ptot) + \
            encode_value_to_32bits(pr1) + \
            encode_value_to_32bits(pr2) + \
            encode_value_to_32bits(pr3) + \
            encode_value_to_32bits(prtot) 

        reg_119 = [int(frequency * 100)]
        # Sungrow modbus stores the values for current as absolute numbers
        if i1 < 0: i1 = -i1
        if i2 < 0: i2 = -i2
        if i3 < 0: i3 = -i3
        reg_97 = [0,0,0,0,0,0]
        reg_97[0] = int(u1 * 10)
        reg_97[1] = int(u2 * 10)
        reg_97[2] = int(u3 * 10)
        reg_97[3] = int(i1 * 100)
        reg_97[4] = int(i2 * 100)
        reg_97[5] = int(i3 * 100)
        reg_10 = [0,0,0,0,0,0,0,0,0,0,0,0]
        lst = encode_value_to_32bits(e_consumed * 100)
        reg_10[0] = lst[0]
        reg_10[1] = lst[1]
        reg_10[2] = lst[0]
        reg_10[3] = lst[1]
        lst = encode_value_to_32bits(e_produced * 100)
        reg_10[10] = lst[0]
        reg_10[11] = lst[1]
        registers = {"reg_10": reg_10,"reg_97": reg_97,"reg_119": reg_119,"reg_356": reg_356}

    return registers


# --------------------------------------------------------------------------- #
# updates the registers from mqtt data
# --------------------------------------------------------------------------- #

def check_registers(context):
    global meter_timeout
    global meter_debug
    global last_updated
    global meter_json_data
    global inverter_interface_type
    global modbus_data
    global modbus_data_previous
    global modbus_same_data_counter

    slave_id = 0
    timestamp_now = int(time.time())
    registers = convert_values_to_register(meter_json_data,inverter_interface_type)

    modbus_data_previous = copy.deepcopy(modbus_data)
    modbus_data = []

    if 'Fronius' in inverter_interface_type:
        if meter_debug: print("reg_258:", registers['reg_258'])
        if meter_debug: print("reg_286:", registers['reg_286'])
        if meter_debug: print("reg_1024:", registers['reg_1024'])
        context[slave_id].setValues(3, 258, registers['reg_258'])
        context[slave_id].setValues(3, 286, registers['reg_286'])
        context[slave_id].setValues(3, 1024, registers['reg_1024'])
        modbus_data = registers['reg_286']

    if 'Sungrow_dtsu666' in inverter_interface_type:
        if meter_debug: print("reg_10:", registers['reg_10'])
        if meter_debug: print("reg_97:", registers['reg_97'])
        if meter_debug: print("reg_119:", registers['reg_119'])
        if meter_debug: print("reg_356:", registers['reg_356'])
        context[slave_id].setValues(3, 10, registers['reg_10'])
        context[slave_id].setValues(3, 97, registers['reg_97'])
        context[slave_id].setValues(3, 119, registers['reg_119'])
        context[slave_id].setValues(3, 356, registers['reg_356'])
        modbus_data = registers['reg_356']

    # check if received data is the same or if it has changed
    modbus_same_data_counter = check_modbus_data_values(modbus_data,modbus_data_previous,modbus_same_data_counter)

    # need to exit ugly, if we want to do backup power
    if (timestamp_now - int(last_updated) > int(meter_timeout)):
        syslog.syslog(syslog.LOG_ERR, "ERROR - Timeout on new data from real meter")
        print("Timeout on new data from real meter, exiting")
        exit(1)

def encode_value_to_32bits(v):
    a = abs(v) // 65536
    b = abs(v) - a * 65536
    if v < 0:
        b = 65535 - b
        a = 65535 - a
    return [int(a), int(b)]


def run_updating_server():
    global meter_debug
    global meter_timeout
    global context
    global inverter_interface_type
    global fake_topic
    global meter_json_data,meter_json_data_previous
    global modbus_data,modbus_data_previous
    global modbus_same_data_counter

    meter_address = 1  # default value
    print("\nStarting meter_handler")
    print("Reading config from file")
    config = json.loads(read_config_file())
    if config:
        try: 
            meter_address = config['meter_address']
        except:
            if "Sungrow_dtsu666" in config['inverter_interface_type']:
                meter_address = 254
            if "Fronius" in config['inverter_interface_type']:
                meter_address = 1
            if "Fronius_ts65a3" in config['inverter_interface_type']:
                meter_address = 1
            if "Fronius_ts5ka3" in config['inverter_interface_type']:
                meter_address = 33
        meter_timeout = config['meter_timeout']
        meter_debug = config['meter_debug']
        mqtt_broker = config['mqtt_broker']
        mqtt_port = config['mqtt_port']
        mqtt_username = config['mqtt_username']
        mqtt_password = config['mqtt_password']
        inverter_interface_type = config['inverter_interface_type']
        if len(sys.argv) < 3:
            inverter_interface = config['inverter_interface']
            fake_unique_id = config['fake_unique_id']
        else:
            try:
                inverter_interface = sys.argv[1]
                fake_unique_id = sys.argv[2]
                if type(inverter_interface_type) is list:
                    # multi inverter split
                    inverter_interface_type = config['inverter_interface_type'][int(fake_unique_id) - 1]
            except IndexError:
                print(
                    "Please use the ip:port or modbus interface as parameter (/dev/ttyxxxx) and unique id as 2nd parameter")
                exit(1)
        if "Fronius_modbus" in inverter_interface_type:
            inverter_interface_type = inverter_interface_type.replace("Fronius_", "Fronius_ts65a3")
        print("Inverter Type:", inverter_interface_type)
        fake_topic = "fake/" + fake_unique_id
        if "1" in config['real_data']:
            fake_topic = "meter/data"

    else:
        exit(1)

    meter_json_data = {}
    meter_json_data_previous = {}
    modbus_data = []
    modbus_data_previous = {}
    modbus_same_data_counter = 0

    # this mqtt client will subscribe
    random_string = str(binascii.b2a_hex(os.urandom(8)))
    mqtt_client2 = paho.Client(client_id="fake_meter_sub_" + fake_unique_id + "_" + random_string, transport="tcp",
                               protocol=paho.MQTTv311, clean_session=True)
    mqtt_client2.on_subscribe = mqtt_on_subscribe
    mqtt_client2.on_connect = mqtt_on_connect
    mqtt_client2.on_message = mqtt_on_message
    mqtt_client2.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client2.connect(mqtt_broker, mqtt_port, keepalive=60)
    mqtt_client2.loop_start()

    context = init_datastore(inverter_interface_type, fake_unique_id,meter_address)
    if not context:
        print("No correct Inverter Type found in config")
        exit(1)
    # ----------------------------------------------------------------------- # 
    # initialize the server information
    # ----------------------------------------------------------------------- # 
    identity = ModbusDeviceIdentification()
    identity.VendorName = 'pymodbus'
    identity.ProductCode = 'PM'
    identity.VendorUrl = 'http://github.com/riptideio/pymodbus/'
    identity.ProductName = 'pymodbus Server'
    identity.ModelName = 'pymodbus Server'

    while last_updated == 0:
        if meter_debug: print("No mqtt data yet for ", fake_topic)
        time.sleep(1)

    syslog.syslog(syslog.LOG_INFO, "Starting modbus server, " + inverter_interface_type + " on " + inverter_interface)
    print("Starting modbus server, ",inverter_interface_type," on ", inverter_interface)
    asyncio.run(run_async_server(context, identity, inverter_interface_type, inverter_interface))
    syslog.syslog(syslog.LOG_INFO, "Modbus server ended")
    print("Server ended")
    exit(1)


async def run_async_server(context, identity, inverter_interface_type, inverter_interface):
    print("Register task for meter data check")
    asyncio.create_task(check_meter_data())
    if re.search(r'_tcp', inverter_interface_type):
        inverter_ip = inverter_interface.split(":")[0]
        inverter_port = int(inverter_interface.split(":")[1])
        print(" modbus tcp server at: ", inverter_ip, ":", inverter_port)
        await StartAsyncTcpServer(context=context, identity=identity, address=(inverter_ip, inverter_port))
    else:
        print(" modbus rtu server at: ", inverter_interface)
        await StartAsyncSerialServer(context=context, framer=ModbusRtuFramer, identity=identity,
                                     port=inverter_interface, timeout=.005, baudrate=9600)


if __name__ == '__main__':
    run_updating_server()
