#!/bin/bash
sudo apt update
sudo apt install mosquitto mosquitto-clients git tmux apache2
sudo systemctl enable mosquitto
echo "type 2 times the password batt2gen24_pass"
sudo mosquitto_passwd -c /etc/mosquitto/.passwd batt2gen24   # type "batt2gen24_pass" when asked for password
sudo echo "listener 1883">/etc/mosquitto/conf.d/auth.conf
sudo echo "allow_anonymous false">>/etc/mosquitto/conf.d/auth.conf
sudo echo "password_file /etc/mosquitto/.passwd">>/etc/mosquitto/conf.d/auth.conf
sudo echo "listener 9001">/etc/mosquitto/conf.d/websockets.conf
sudo echo "protocol websockets">>/etc/mosquitto/conf.d/websockets.conf
sudo systemctl restart mosquitto
sudo mkdir /opt/smartmeter
cd /opt
sudo git clone https://gitlab.com/pelle8/smartmeter.git
cd /opt/smartmeter/code
sudo chmod +x *.sh
sudo ln -s /opt/smartmeter/www/stats.html      /var/www/html/stats.html
cd /opt/smartmeter/systemd
sudo cp * /etc/systemd/system
sudo systemctl daemon-reload
sudo systemctl enable fake_meter
sudo systemctl enable smart_meter
sudo systemctl enable smart_power
sudo systemctl start fake_meter
sudo systemctl start smart_meter
sudo systemctl start smart_power
