#!/usr/bin/env python
"""
Feeds fake meters with power values, distributes the power among inverters //Per Carlen
"""

import json
import os
import sys
import time
from datetime import datetime

import paho.mqtt.client as paho
import pytz
import yaml
from pymodbus.client import ModbusTcpClient as ModbusClient

inverter_data = []
client = []
config = []
last_updated = ""
grid_ignore = False

reg_20480 = []
reg_63 = []
reg_356 = []
reg_119 = []
reg_97 = []
reg_10 = []

line = ""
reg_time = [0, 0, 0]
reg_181 = [0, 0, 0]
reg_281 = [0, 0, 0]
list_p_consume = [0, 0, 0]
list_p_produce = [0, 0, 0]
wchamax = 0
max_charge = 8000
meter_debug = True

from typing import Dict


class InverterController:
  def __init__(self):
    self.config = self.read_config_file('config.yaml')

  @staticmethod
  def read_config_file(filename: str) -> Dict:
    config = ""
    filename = "power_controller.yaml"
    path = "/etc/smartmeter/"
    if os.path.isfile(path + filename):
      filename = path + filename
    print("Reading from:", filename)
    try:
      with open(filename, 'r') as file:
        config = yaml.safe_load(file)
    except:
      print("ERROR: No config file - ", filename)
      exit(1)
    return config


# --------------------------------------------------------------------------- #
# configure logging
# --------------------------------------------------------------------------- #


# if meter_debug:
#    logging.basicConfig()
#    log = logging.getLogger()
#    log.setLevel(logging.DEBUG)


def get_value_from_32bits(r1, r2):
  v = r1 + (r2 & 32767) * 65536
  if r2 > 32768:
    v = - ((65535 - r1) + (65535 - r2) * 65536)
  return v


def mqtt_on_message(mqtt_client, userdata, message, tmp=None):
  global config
  global grid_ignore
  global last_updated
  global reg_20480
  global reg_63
  global reg_356
  global reg_119
  global reg_97
  global reg_10
  global list_p_consume
  global list_p_produce

  if config['debug']:
    print(" Received message " + str(message.payload)
          + " on topic '" + message.topic
          + "' with QoS " + str(message.qos))

  # TODO: add Fronius values...

  print(message.topic)
  if message.topic == "meter/data":
    if config['debug']: print("Got mqtt update")
    json_data = json.loads(message.payload)
    if type(json_data) is dict or type(json_data) is list:
      last_updated = json_data['last_updated']
      p = json_data['pt']
      p_consume = 0
      p_produce = 0
      if p > 0: p_consume = p
      if p < 0: p_produce = -p
      list_p_consume = shift(list_p_consume, p_consume)
      list_p_produce = shift(list_p_produce, p_produce)
      if len(config) > 0:
        p_inv = adjust_power(p, config, grid_ignore, json_data)


def mqtt_on_subscribe(client, userdata, mid, granted_qos):
  # print(" Received message " + str(client)
  #    + "' with QoS " + str(granted_qos))
  pass


def mqtt_on_connect(client, userdata, flags, rc):
  if rc != 0:
    print("MQTT connection problem")
    client.connected_flag = False
  else:
    print("MQTT client connected:" + str(client))
    client.connected_flag = True
    client.subscribe("meter/data", 2)


def mqtt_on_publish(mqtt_client, userdata, result):
  # print("data published:",result)
  pass


def publish(mqtt_client, pub_var, topic):
  global config

  json_string = json.dumps(pub_var)
  ret = mqtt_client.publish(topic, json_string)
  if config['debug']:  print("publishing in:", topic)


def run_mqtt_setup(config):
  global meter_data
  global mqtt_client1
  global mqtt_client2
  global reg_10
  global meter_debug

  print("\nStarting mqtt handler")
  if config:
    mqtt_broker = config['mqtt_broker']
    mqtt_port = config['mqtt_port']
    mqtt_username = config['mqtt_username']
    mqtt_password = config['mqtt_password']
  else:
    exit(1)

  # this mqtt client will publish
  mqtt_client1 = paho.Client(client_id="power_controller_publisher", transport="tcp", protocol=paho.MQTTv311,
                             clean_session=True)
  mqtt_client1.on_publish = mqtt_on_publish
  mqtt_client1.on_connect = mqtt_on_connect
  mqtt_client1.username_pw_set(mqtt_username, mqtt_password)
  mqtt_client1.connect(mqtt_broker, mqtt_port, keepalive=60)
  mqtt_client1.loop_start()

  # this mqtt client will subscribe
  mqtt_client2 = paho.Client(client_id="power_controller", transport="tcp", protocol=paho.MQTTv311, clean_session=True)
  mqtt_client2.on_subscribe = mqtt_on_subscribe
  mqtt_client2.on_connect = mqtt_on_connect
  mqtt_client2.on_message = mqtt_on_message
  mqtt_client2.username_pw_set(mqtt_username, mqtt_password)
  mqtt_client2.connect(mqtt_broker, mqtt_port, keepalive=60)
  mqtt_client2.loop_start()


def write_error(err):
  f = open("/tmp/error.log", "a")
  f.write(err)
  f.close()


def write_json(consumed, produced, power_now_grid, inverter_power):
  date_now = datetime.today().strftime('%Y%m%d')

  energy_data = {'date_now': date_now, 'e_tot_produced': produced, 'e_tot_consumed': consumed,
                 'power_now_grid': power_now_grid, 'inverter_power': inverter_power}
  if config['debug']: print("Json:", energy_data)
  f = open("/var/www/html/gridpoint.json", "w")
  f.write(json.dumps(energy_data))
  f.close()
  f = open("/tmp/energy.log", "a")
  f.write(json.dumps(energy_data) + "\n")
  f.close()


def shift(reg, v):
  i = 1
  while i < len(reg):
    reg[i - 1] = reg[i]
    i += 1
  reg[len(reg) - 1] = v
  return reg


def read_reg(client, rtype, register, count, id):
  rr = []
  ret = ""
  counter = 0
  response = False
  while counter < 4 and not response:
    try:
      if rtype == "holding":
        rr = client.read_holding_registers(register, count, id)
      if rtype == "input":
        rr = client.read_input_registers(register, count, id)
      response = True
    except Exception as e:
      print(e)
      pass
    counter += 1
  try:
    if count == 1:
      ret = rr.registers[0]
    else:
      ret = rr.registers
  except:
    return ""
  return ret


def init_inverter(client, i):
  global inverter_data
  print("Getting initial data from:", inverter_data[i]['name'])
  if inverter_data[i]['type'] == "Gen24":
    try:
      v = read_reg(client, "holding", 40351, 1, 1) / 100
      if v > 0 and v < 100:
        inverter_data[i]['soc'] = v
        print(" Got initial data from:", inverter_data[i]['name'], " - soc:", v)
        inverter_data[i]['pv'] = 0
        wchamax = 0
        rr = rr_write(client, 40348, 0, 0x01)  # limit
        rr = rr_write(client, 40355, 0, 0x01)  # outwrte
        rr = rr_write(client, 40356, 0, 0x01)  # inwrte
      else:
        print(" Weird soc from:", inverter_data[i]['name'])
        return False
    except:
      print(" No data from:", inverter_data[i]['name'])
      pass
      return False
  if inverter_data[i]['type'] == "Sungrow":
    try:
      v = read_reg(client, "input", 13022, 1, 1) / 10
      inverter_data[i]['soc'] = v
      print(" Got initial data from:", inverter_data[i]['name'], " - soc:", v)
      # EMS mode 2 = Forced mode 
      rr = rr_write(client, 13049, 2, 0x01)
      #rr = rr_write(client, 13050, 0xcc, 0x01)
    except:
      print(" No data from:", inverter_data[i]['name'])
      pass
      return False
  return True


def connect_to_inverter(client, inverter_config):
  global inverter_data

  if not inverter_config['name'] in inverter_data:
    inverter_data.append(inverter_config)

  print("Connecting to:", inverter_config['name'])
  i = - 1
  for index, n in enumerate(inverter_data):
    if n['name'] == inverter_config['name']: i = index

  if i > -1:
    connected = False
    counter = 0
    while not connected and counter < 5:
      try:
        client.connect()
        counter += 1
        try:
          if inverter_config['type'] == "Gen24":
            rr = client.read_holding_registers(40345, 1, 0x01)
          if inverter_config['type'] == "Sungrow":
            rr = client.read_input_registers(13022, 1, 0x01)
          connected = True
        except:
          print("Couldn't read tcp modbus from", inverter_config['ip'])
          print("Retrying to connect in 0.5s")
          time.sleep(0.5)
      except:
        print("Retrying to connect in 1s")
        time.sleep(1)
    inverter_data[i]['connected'] = connected
    if connected: init_inverter(client, i)
  return connected


def rr_write(client, reg, value, unit_id):
  try:
    rr = client.write_registers(reg, value, unit_id)
  except Exception as e:
    print(e)
    rr = False
    pass

  return rr


def check_inverter_status(client, config):
  global inverter_data

  print("--------------------------------")
  print("Will check inverter status")
  for j, inverter in enumerate(config['inverters']):
    i = - 1
    for index, n in enumerate(inverter_data):
      if n['name'] == inverter['name']: i = index

    if i > -1:
      # read current settings from inverter, read registers in a chunk to improve performance (especially Sungrow is awfully slow in responding)
      inverter_data[i]['active'] = True
      if inverter_data[i]['type'] == "Sungrow":
        try:
          v = read_reg(client[i], "input", 13022, 1, 1)
          if v != "":
            inverter_data[i]['soc'] = v / 10
            inverter_data[i]['connected'] = True
          else:
            inverter_data[i]['connected'] = False
        except Exception as e:
          print(" Couldn't read from tcp modbus for:", inverter_data[i]['name'])
          try:
            client[i].close()
          except Exception as e:
            pass
          inverter_data[i]['connected'] = False
          pass

      if inverter_data[i]['type'] == "Gen24":
        inverter_data[i]['active'] = True
        try:
          print(i)  # p<0 discharge
          v = read_reg(client[i], "holding", 40351, 1, 1)
          if v != "" and v > 0 and v < 10000:
            inverter_data[i]['soc'] = v / 100
            inverter_data[i]['connected'] = True
          else:
            inverter_data[i]['connected'] = False
        except Exception as e:
          print(" Couldn't read from tcp modbus for:", inverter_data[i]['name'])
          try:
            client[i].close()
          except Exception as e:
            pass
          inverter_data[i]['connected'] = False
          pass
      if 'soc' in inverter_data[i]:
        soc = inverter_data[i]['soc']
      else:
        soc = 'N/A'
      print("Inverter:",inverter_data[i]['name'],"SoC:",inverter_data[i]['soc'],"Alive:",inverter_data[i]['connected'])


def adjust_power(p, config, grid_ignore, json_data):
  global inverter_data

  print("--------------------------------")
  print("Will adjust overall P to:", str(p), " ,ignoring grid:", grid_ignore)  # p<0 discharge

  num_active_inverters = 0
  for n in inverter_data:
    if n['connected']: num_active_inverters += 1

  print("Alive: ", num_active_inverters)
  if num_active_inverters == 0:
    print("No inverters are connected, can't distribute power")
    return

  # Check SoC and state and disable if out of range
  for j, inverter in enumerate(config['inverters']):
    i = - 1
    for index, n in enumerate(inverter_data):
      if n['name'] == inverter['name']: i = index

    if inverter_data[i]['connected']:
      if grid_ignore:
        p_new_inverter_power = inverter_data[i]['max_power']
      else:
        p_new_inverter_power = p / num_active_inverters
      if 'soc' in inverter_data[i]:
        if inverter_data[i]['soc'] == 0:  # Fishy error when tcp conn is messy, retart the whole thing...
          exit(1)
        inverter_data[i]['active'] = True
        if config.get('disable_soc') is None:
          if p_new_inverter_power > 0 and inverter_data[i]['soc'] < inverter_data[i]['soc_low']:  # Charge
            print(" Disabling inverter", inverter_data[i]['name'], "for discharging due to low soc.")
            ret = adjust_inverter_power(0, i, json_data)
            num_active_inverters -= 1
            inverter_data[i]['active'] = False
          if p_new_inverter_power < 0 and inverter_data[i]['soc'] > inverter_data[i]['soc_high']:  # Discharge
            print("p_new_inverter_power:", p_new_inverter_power)
            print(" Disabling inverter", inverter_data[i]['name'], "for charging due to high soc.")
            ret = adjust_inverter_power(0, i, json_data)
            inverter_data[i]['active'] = False
            num_active_inverters -= 1

  print(" P adjust for:", num_active_inverters, " active inverter(s)")

  # get real power from inverters in order to compute max power

  if num_active_inverters > 0:
    # Distribute the power depending on max power, compute total "available" power first
    p_max_active_inverters = 1
    for i in inverter_data:
      if 'active' in i and i['active']:
        p_max_active_inverters += i['max_power']

    # Adjust the power on the inverters...
    for j, inverter in enumerate(config['inverters']):
      i = - 1
      for index, n in enumerate(inverter_data):
        if n['name'] == inverter['name']: i = index

      if inverter_data[i]['connected']:
        if grid_ignore:
          p_new_inverter_power = inverter_data[i]['max_power']
        else:
          p_new_inverter_power = int(p * inverter_data[i]['max_power'] / p_max_active_inverters)
        #        if abs(p_new_inverter_power) > inverter_data[i]['max_power']:
        #          if p_new_inverter_power > 0: p_new_inverter_power = inverter_data[i]['max_power']
        #          if p_new_inverter_power < 0: p_new_inverter_power = -inverter_data[i]['max_power']
        if 'active' in inverter_data[i] and inverter_data[i]['active']:
          adjust_inverter_power(p_new_inverter_power, i, json_data)
        else:
          if not 'active' in inverter_data[i]:
            adjust_inverter_power(p_new_inverter_power, i, json_data)


def adjust_inverter_power(p_inverter_new, index, json_data):
  global inverter_data
  global config

  # Negative values means consuming from grid
  if 'soc' in inverter_data[index]:
    soc = inverter_data[index]['soc']
  else:
    soc = "N/A"
  print(" Will adjust to :", str(p_inverter_new), " for ", inverter_data[index]['name'], ", soc:", soc, " (low:",
        inverter_data[index]['soc_low'], ", high:", inverter_data[index]['soc_high'], ")")
  if config['debug']: print("idata:", inverter_data[index])
  if config['debug']: print("meter data:", json_data)
  # TODO: fix all values for P and I?
  json_data["pt"] = p_inverter_new
  if config['debug']: print("new meter data:", json_data)
  topic = "fake/" + inverter_data[index]["fake_unique_id"]
  publish(mqtt_client1, json_data, topic)
  return

  if p == -99999:  # Out of time control
    if config['debug']: print("resetting registers")
    if inverter_data[index]['type'] == "Gen24":
      rr = rr_write(client[index], 40348, 0, 0x01)  # limit
      rr = rr_write(client[index], 40355, 0, 0x01)  # outwrte
      rr = rr_write(client[index], 40356, 0, 0x01)  # inwrte

    return

  if inverter_data[index]['type'] == "Sungrow":
    wrte = inverter_data[index]['registers'][1]
    reg = 0
    if p_inverter_new < 0:  # discharge battery
      reg = int(-p_inverter_new)
      registers = [0xbb, reg]
      rr = rr_write(client[index], 13050, registers, 0x01)  # aa bb cc charge/disch/stop

    if p_inverter_new > 0:  # charge battery
      reg = int(p_inverter_new)
      registers = [0xaa, reg]
      rr = rr_write(client[index], 13050, registers, 0x01)  # aa bb cc charge/disch/stop

    if p_inverter_new == 0:  # reset all
      registers = [0xcc, reg]
      rr = rr_write(client[index], 13050, registers, 0x01)  # aa bb cc charge/disch/stop

    if config['debug']: print("new_reg:", reg, "Current:", wrte)

  if inverter_data[index]['type'] == "Gen24":
    wchamax = inverter_data[index]['registers'][0]
    outwrte = inverter_data[index]['registers'][10]
    inwrte = inverter_data[index]['registers'][11]
    ac_power = inverter_data[index]['ac_power']
    pv = inverter_data[index]['pv']
    p_batt = 0
    if outwrte > 0:  # Charging
      p_batt = (65535 - outwrte) / 10000 * wchamax
    if inwrte > 0:  # Discharging
      p_batt = -(65535 - inwrte) / 10000 * wchamax

    p_batt_new = pv + p_inverter_new

    # p_batt_new = 200
    print("  pv:", pv, "P batt:", p_batt, "P batt new:", p_batt_new)
    reg = 0
    if reg == 0:
      if p_batt_new < 0 and wchamax > 0:  # discharge battery
        reg = int(65535 - (-p_batt_new * 10000 / wchamax))
        rr = rr_write(client[index], 40348, 1, 0x01)  # disch limit
        rr = rr_write(client[index], 40355, 0, 0x01)  # outwrte
        rr = rr_write(client[index], 40356, reg, 0x01)  # inwrte

      if p_batt_new > 0 and wchamax > 0:  # charge battery
        reg = int(65535 - (p_batt_new * 10000 / wchamax))
        rr = rr_write(client[index], 40348, 2, 0x01)  # ch limit
        rr = rr_write(client[index], 40355, reg, 0x01)  # outwrte
        rr = rr_write(client[index], 40356, 0, 0x01)  # inwrte

      if p_batt_new == 0 or wchamax == 0:  # reset all
        rr = rr_write(client[index], 40348, 0, 0x01)  # limit
        rr = rr_write(client[index], 40355, 0, 0x01)  # outwrte
        rr = rr_write(client[index], 40356, 0, 0x01)  # inwrte

    if config['debug']: print("new_reg:", reg, "Current out:", outwrte, " in:", inwrte)
  return reg


print("\nStarting power controller")
print("Reading config from file")
inverter_controller = InverterController()
config = inverter_controller.config
if config['debug']: print(config['inverters'])
for i, inverter in enumerate(config['inverters']):
  if config['debug']: print(i, inverter['name'])
  client.append(ModbusClient(inverter['ip'], port=502))
  connect_to_inverter(client[-1], inverter)

if len(sys.argv) > 1:
  if sys.argv[1] == "clear":
    print("cleared registers in all inverters")
    exit(1)

run_mqtt_setup(config)
while last_updated == "":
  print("Waiting for mqtt data")
  time.sleep(1)

while 1:
  now = datetime.now(pytz.timezone('Europe/Stockholm'))
  ts = int(now.strftime("%S")) + 60 * int(now.strftime("%M")) + 3600 * int(now.strftime("%H"))

  if config['debug']: print("Time: " + str(ts))
  grid_ignore = False
  if config['time_control']:
    for time_control_item in config['time_control']:
      start = time_control_item['start_time']
      end = time_control_item['end_time']
      start_time = int(start[0:2]) * 3600 + int(start[3:5]) * 60 + int(start[6:8])
      end_time = int(end[0:2]) * 3600 + int(end[3:5]) * 60 + int(end[6:8])
      if start_time < ts and end_time > ts:
        p = time_control_item['power']
        if config['debug']: print("Self optimization disabled, static power per inverter:", p)
        grid_ignore = True

  rc = check_inverter_status(client, config)
  # p_inv = adjust_power(p,client,config,grid_ignore)
  # write_json(reg_181[len(reg_181)-1],reg_281[len(reg_281)-1],p_now,p_inv)

  # Check if all inverters are still connected
  for i1, inverter in enumerate(config['inverters']):
    for i2, i_data in enumerate(inverter_data):
      if i_data['name'] == inverter['name']:
        if not i_data['connected']:
          connect_to_inverter(client[i1], inverter)
  time.sleep(5)
