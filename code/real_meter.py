#!/usr/bin/env python3
"""
Pymodbus Client 
- Reads data from Sungrow or Fronius Smart Meter on modbus rtu and publishes with mqtt // Per Carlen
"""
import sys
import asyncio
import json
import os
import socket
import time
from datetime import datetime
from typing import Dict
import copy
import paho.mqtt.client as paho
import yaml
from pymodbus.client import AsyncModbusSerialClient


class MeterHandler:

    def __init__(self):
        self.config = self.read_config_file('real_meter.yaml')

    @staticmethod
    def read_config_file(filename: str) -> Dict:
        config = ""
        path = "/etc/smartmeter/"
        if os.path.isfile(path + filename):
            filename = path + filename
        print("Reading from:", filename)
        try:
            with open(filename, 'r') as file:
                config = yaml.safe_load(file)
        except:
            print("ERROR: No config file - ", filename)
            exit(1)
        return config


def print2log(msg):
    return msg


def get_rr(rr):
    ret = []
    for x in rr.registers:
        ret.append(x)
    return ret


def print_rr(rr, var_type):
    ret = ""
    for x in rr.registers:
        if var_type == "hex":
            s = x.to_bytes(2, "big").hex()
            ret += print2log(s + " ")
        if var_type == "dec":
            ret += print2log(str(x) + ",")
        if var_type == "str":
            s = x.to_bytes(2, "big")
            ret += print2log(str(chr(s[0])) + str(chr(s[1])) + ",")
    ret += print2log("")
    return ret.rstrip(',')


async def read(reg, count, unit_id, var_type):
    global meter_debug
    global client

    ret = []
    retry_counter = 1
    while retry_counter > 0:
        rr = await client.read_holding_registers(reg, count, unit_id)
        if not rr.isError():
            if meter_debug: print(rr.registers)
            ret = get_rr(rr)
            retry_counter = 0
        if retry_counter > 0:
            asyncio.sleep(0.5)
        retry_counter -= 1
    return ret


def mqtt_on_publish(mqtt_client, userdata, result):
    global meter_debug

    if meter_debug: print("Data published counter:",result)


def mqtt_on_connect(client, userdata, flags, rc):
    if rc != 0:
        print("MQTT connection problem")
        client.connected_flag = False
    else:
        print("MQTT client connected:" + str(client))
        client.connected_flag = True


def publish(client, msg_string, topic):
    ret = client.publish(topic, msg_string)

# Fronius coding...
def fronius_decode_value_32bits(register_array, offset):
    v = register_array[offset] + (register_array[offset + 1] & 32767) * 65536
    if register_array[offset + 1] > 32768:
        v = - ((65535 - register_array[offset]) + (65535 - register_array[offset + 1]) * 65536)
    return v

# Fronius coding...
def fronius_decode_value_64bits(register_array, offset):
    return fronius_decode_value_32bits(register_array, offset) + fronius_decode_value_32bits(register_array, offset + 2) / 1000

# Sungrow coding....
def get_value_from_32bits(r1, r2):
    v = r1 + (r2 & 32767) * 65536
    if r2 > 32768:
        v = - ((65535 - r1) + (65535 - r2) * 65536)
    return v


def divide_by_inverters(result_dict,inverter_count):
    ret_dict = {}
    for key in result_dict:
        v = result_dict[key]
        if key == "e_consumed" or key == "e_produced" or key == "er_consumed" or key == "er_produced" or key == "i1" or key == "i2" or \
            key == "i3" or key == "p1" or key == "p2" or key == "p3" or key == "pr1" or key == "pr2" or key == "pr3" or \
            key == "pt" or key == "prt":
            v = v / inverter_count
        ret_dict[key] = v
    return ret_dict

# Here comes the decoders for the smartmeters...
def decode_values(meter_type,register_hash):
    
    if "Fronius" in meter_type:
        reg_258 = register_hash['r258']
        reg_286 = register_hash['r286']
        reg_1024 = register_hash['r1024']
        p1 = round(fronius_decode_value_32bits(reg_286, 6)   / 10 ,2)
        p2 = round(fronius_decode_value_32bits(reg_286, 20)  / 10 ,2)
        p3 = round(fronius_decode_value_32bits(reg_286, 34)  / 10 ,2)
        pt = round(fronius_decode_value_32bits(reg_258, 4)   / 10 ,2)
        pr1 = round(fronius_decode_value_32bits(reg_286, 10) / 10 ,2)
        pr2 = round(fronius_decode_value_32bits(reg_286, 24) / 10 ,2)
        pr3 = round(fronius_decode_value_32bits(reg_286, 38) / 10 ,2)
        prt = round(fronius_decode_value_32bits(reg_258, 8)  / 10 ,2)

        pa1 = round(fronius_decode_value_32bits(reg_286, 8)  / 10 ,2)
        pa2 = round(fronius_decode_value_32bits(reg_286, 22) / 10 ,2)
        pa3 = round(fronius_decode_value_32bits(reg_286, 36) / 10 ,2)
        pat = round(fronius_decode_value_32bits(reg_258, 6)  / 10 ,2)
        pf1 = round(fronius_decode_value_32bits(reg_286, 12) / 1000 ,4)
        pf2 = round(fronius_decode_value_32bits(reg_286, 26) / 1000 ,4)
        pf3 = round(fronius_decode_value_32bits(reg_286, 40) / 1000 ,4)
        pft = round(fronius_decode_value_32bits(reg_258, 10) / 1000 ,4)

        f = fronius_decode_value_32bits(reg_258, 14) / 10
        u1 = fronius_decode_value_32bits(reg_286, 2) / 10
        u2 = fronius_decode_value_32bits(reg_286, 16) / 10
        u3 = fronius_decode_value_32bits(reg_286, 30) / 10
        i1 = round(fronius_decode_value_32bits(reg_286, 4)  / 1000 ,2)
        i2 = round(fronius_decode_value_32bits(reg_286, 18)  / 1000 ,2)
        i3 = round(fronius_decode_value_32bits(reg_286, 32)  / 1000 ,2)
        ec = fronius_decode_value_64bits(reg_1024, 0)
        erc = fronius_decode_value_64bits(reg_1024, 4) 
        ep = fronius_decode_value_64bits(reg_1024, 8) 
        erp = fronius_decode_value_64bits(reg_1024, 12) 


    if meter_type == "Sungrow_dtsu666":
        reg_356 = register_hash['r356']
        reg_119 = register_hash['r119']
        reg_97 = register_hash['r97']
        reg_10 = register_hash['r10']

        p1 = round(get_value_from_32bits(reg_356[1], reg_356[0])  ,2)
        p2 = round(get_value_from_32bits(reg_356[3], reg_356[2])  ,2)
        p3 = round(get_value_from_32bits(reg_356[5], reg_356[4])  ,2)
        pt = round(get_value_from_32bits(reg_356[7], reg_356[6])  ,2)
        pr1 = round(get_value_from_32bits(reg_356[9], reg_356[8])  ,2)
        pr2 = round(get_value_from_32bits(reg_356[11], reg_356[10])  ,2)
        pr3 = round(get_value_from_32bits(reg_356[13], reg_356[12])  ,2)
        prt = round(get_value_from_32bits(reg_356[15], reg_356[14])  ,2)

        pa1 = abs(p1) + abs(pr1)
        pa2 = abs(p2) + abs(pr2)
        pa3 = abs(p3) + abs(pr3)
        pat = abs(pt) + abs(prt)
        pf1 = round(abs(p1) / abs(pa1) ,4)
        pf2 = round(abs(p2) / abs(pa2) ,4)
        pf3 = round(abs(p3) / abs(pa3) ,4)
        pft = round(abs(pt) / abs(pat) ,4)

        f = reg_119[0] / 100
        u1 = reg_97[0] / 10
        u2 = reg_97[1] / 10
        u3 = reg_97[2] / 10
        i1 = round(reg_97[3] / 100  ,2)
        i2 = round(reg_97[4] / 100  ,2)
        i3 = round(reg_97[5] / 100  ,2)
        # The values for current are absolute from Sungrow modbus registers, but in mqtt we want to insert the real values...
        if p1 < 0: i1 = -i1
        if p2 < 0: i2 = -i2
        if p3 < 0: i3 = -i3
        ec = get_value_from_32bits(reg_10[1], reg_10[0]) / 100
        ep = get_value_from_32bits(reg_10[11], reg_10[10]) / 100
        erc = 0
        erp = 0


    ret_hash = {'e_consumed': ec, 'e_produced': ep, 'er_consumed': erc, 'er_produced': erp, 'f': f, 'u1': u1,
                'u2': u2, 'u3': u3, 'i1': i1, 'i2': i2,'i3': i3, 
                'pa1': pa1, 'pa2': pa2, 'pa3': pa3, 'pat': pat, 
                'pf1': pf1, 'pf2': pf2, 'pf3': pf3, 'pft': pft, 
                'p1': p1, 'p2': p2, 'p3': p3, 'pt': pt, 'pr1': pr1, 'pr2': pr2, 'pr3': pr3, 'prt': prt}
    return ret_hash


async def read_from_smart_meter(meter_type,meter_address):
    if meter_type == "Sungrow_dtsu666":
        return await read_from_dtsu666(meter_address)
    if meter_type == "Fronius_ts65a3":
        return await read_from_Fronius_ts65a3(meter_address)
    if meter_type == "Fronius_ts5ka3":
        return await read_from_Fronius_ts65a3(meter_address)
    return


async def read_from_Fronius_ts65a3(addr):
    global meter_debug, dt_string
    ret_hash = {}
    if meter_debug: print(dt_string + " - Reading from Fronius_ts65a3 Smart Meter ")
    reg_258 = await read(258, 16, addr, "dec")
    reg_286 = await read(286, 42, addr, "dec")
    reg_1024 = await read(1024, 16, addr, "dec")

    if len(reg_258) > 15 \
            and len(reg_286) > 41 \
            and len(reg_1024) > 15:
        ret = decode_values("Fronius_ts65a3",{ "r258":reg_258,"r286":reg_286,"r1024":reg_1024 })
        ret_hash = ret | {'registers': {"r258":reg_258,"r286":reg_286,"r1024":reg_1024 }}

    return ret_hash

async def read_from_Fronius_ts5ka3(addr):
    ret_hash = {}
    return ret_hash


async def read_from_dtsu666(addr):
    global meter_debug

    ret_hash = {}
    if meter_debug: print(dt_string + "Reading from Sungrow_dtsu666 Smart Meter ")
    reg_20480 = await read(20480, 1, addr, "dec")
    reg_63 = await read(63, 1, addr, "dec")
    reg_356 = await read(356, 16, addr, "dec")
    reg_119 = await read(119, 1, addr, "dec")
    reg_97 = await read(97, 6, addr, "dec")
    reg_10 = await read(10, 12, addr, "dec")

    if len(reg_20480) > 0 and len(reg_63) > 0 and len(reg_356) > 0 and len(reg_119) > 0 and len(reg_97) > 0 and len(reg_10) > 0:
        ret = decode_values("Sungrow_dtsu666",{ "r20480":reg_20480,"r63":reg_63,"r356":reg_356,"r119":reg_119,"r97":reg_97,"r10":reg_10 })
        ret_hash = ret | {'registers': { 'r20480': reg_20480, 'r63': reg_63, 'r356': reg_356, 'r119': reg_119, 'r97': reg_97, 'r10': reg_10 }}
    return ret_hash


async def run_async_client(command):
    global meter_debug
    global client
    global mqtt_client_smarthome
    global smarthome
    global timestamp, dt_string

    print("\nStarting handler of real meter")
    print("Reading config from file")
    meter_handler = MeterHandler()
    config = meter_handler.config
    if config:
        if config['meter_type'] == "Fronius":
            config['meter_type'] = "Fronius_ts65a3"
        try: 
            meter_address = config['meter_address']
        except:
            if config['meter_type'] == "Sungrow_dtsu666":
                meter_address = 254
            if config['meter_type'] == "Fronius_ts65a3":
                meter_address = 1
            if config['meter_type'] == "Fronius_ts5ka3":
                meter_address = 33
        meter_probe_interval = config['meter_probe_interval']
        meter_debug = config['meter_debug']
        meter_interface = config['meter_interface']
        inverter_count = config['inverter_count']
        if isinstance(inverter_count, str):
            if not inverter_count.isnumeric:
                inverter_count = 1
            else:
                inverter_count = int(inverter_count)
        mqtt_broker = config['mqtt_broker']
        mqtt_port = config['mqtt_port']
        mqtt_username = config['mqtt_username']
        mqtt_password = config['mqtt_password']
    else:
        exit(1)

    print("Meter:",config['meter_type']," on address:",meter_address,",interface:", meter_interface)

    client = AsyncModbusSerialClient(
        method='rtu',
        port=meter_interface,
        baudrate=9600,
        timeout=1,
        parity='N',
        stopbits=1,
        bytesize=8
    )

    await client.connect()
    # test client is connected
    assert client.connected

    if command == "read":
        print("Reading registers...")
        reg_20480 = await read(20480, 1, meter_address, "dec")
        print("REG:",reg_20480)
        exit(0)

    # this mqtt client will publish
    mqtt_client1 = paho.Client(client_id="meter_pub", transport="tcp", protocol=paho.MQTTv311, clean_session=True)
    mqtt_client1.on_publish = mqtt_on_publish
    mqtt_client1.on_connect = mqtt_on_connect
    mqtt_client1.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client1.connect(mqtt_broker, mqtt_port, keepalive=60)
    mqtt_client1.loop_start()

    smarthome = False
    try:
        mqtt_smarthome_broker = config['mqtt_smarthome_broker']
        mqtt_smarthome_port = config['mqtt_smarthome_port']
        mqtt_smarthome_username = config['mqtt_smarthome_username']
        mqtt_smarthome_password = config['mqtt_smarthome_password']
        smarthome = True
    except KeyError:
        print("INFO: No Smarthome MQTT defined")

    mqtt_client_smarthome = ""

    if smarthome:
        mqtt_client_smarthome = paho.Client(client_id=socket.gethostname(), transport="tcp",
                                            protocol=paho.MQTTv311,
                                            clean_session=True)
        mqtt_client_smarthome.on_publish = mqtt_on_publish
        mqtt_client_smarthome.on_connect = mqtt_on_connect
        mqtt_client_smarthome.username_pw_set(mqtt_smarthome_username, mqtt_smarthome_password)
        mqtt_client_smarthome.connect(mqtt_smarthome_broker, mqtt_smarthome_port, keepalive=60)
        mqtt_client_smarthome.loop_start()

    try:
        while True:
            timestamp = str(int(time.time()))
            now = datetime.now()
            dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
            result_dict = await read_from_smart_meter(config['meter_type'],meter_address)
            if result_dict:
                result_dict['last_updated'] = timestamp
                if smarthome:
                    result_dict2 = copy.deepcopy(result_dict)
                    data_to_public = {"meter/data": result_dict2}
                    del data_to_public["meter/data"]['registers']
                    json_string = json.dumps(
                        data_to_public,
                        separators=(',', ':'))
                    if meter_debug: print("Smarthome Json:", json_string)
                    mqtt_client_smarthome.publish('tele/smartmeter_' + socket.gethostname() + '/STATE', json_string)
                inverter_dict = divide_by_inverters(result_dict,inverter_count)

                json_string = json.dumps(inverter_dict, separators=(',', ':'))
                if meter_debug: print("Json:", json_string)
                rc = publish(mqtt_client1, json_string, "meter/data")

                # Nicer keys...not used yet
                # smart_meter_mqtt = {'Time': timestamp,
                #         'Current AC Phase 1': fronius_decode_value_32bits(reg_286, 4) / 10,
                #         'Current AC Phase 2': fronius_decode_value_32bits(reg_286, 22) / 10,
                #         'Current AC Phase 3': fronius_decode_value_32bits(reg_286, 36) / 10,
                #         'Frequency_Phase_Average': fronius_decode_value_32bits(reg_258, 14) / 10,
                #         'PowerApparent S Phase 1': fronius_decode_value_32bits(reg_286, 10) / 10,
                #         'PowerApparent S Phase 2': fronius_decode_value_32bits(reg_286, 24) / 10,
                #         'PowerApparent S Phase 3': fronius_decode_value_32bits(reg_286, 38) / 10,
                #         'PowerApparent S Sum': fronius_decode_value_32bits(reg_258, 4) / 10,
                #         'PowerReal P Phase 1': fronius_decode_value_32bits(reg_286, 6) / 10,
                #         'PowerReal P Phase 2': fronius_decode_value_32bits(reg_286, 20) / 10,
                #         'PowerReal P Phase 3': fronius_decode_value_32bits(reg_286, 34) / 10,
                #         'Voltage AC Phase 1': fronius_decode_value_32bits(reg_286, 2) / 10,
                #         'Voltage AC Phase 2': fronius_decode_value_32bits(reg_286, 16) / 10,
                #         'Voltage AC Phase 3': fronius_decode_value_32bits(reg_286, 30) / 10,
                #         'Voltage_AC_PhaseToPhase_12': fronius_decode_value_32bits(reg_286, 0) / 10,
                #         'Voltage_AC_PhaseToPhase_23': fronius_decode_value_32bits(reg_286, 14) / 10,
                #         'Voltage_AC_PhaseToPhase_31': fronius_decode_value_32bits(reg_286, 28) / 10,
                #         'Model': 'Fronius_ts65a3 Fake Smartmeter'}

            else:
                if meter_debug: print("Error reading from meter")
            await asyncio.sleep(meter_probe_interval)
    except KeyboardInterrupt:
        pass
    client.close()


if __name__ == "__main__":
    command = ""
    if sys.argv[len(sys.argv)-1] == "read":
        command = "read"
    asyncio.run(run_async_client(command))
