# Code

There are basically two components involved in the setup:
- real_meter: that connects to the physical meter, retieves values and publishes them on MQTT
- fake_meter: receives fake values from MQTT and makes them available to the inverter, using modbus rtu or tcp

In addition to these(optional):
- power_controller: receives the "real" values from MQTT, checks the status of the inverters, distributes power and publishes inverter individual values on MQTT 


# Installation

- install the python from code directory: `code $ pip install -r requirements.txt`
- install and configure mqtt server (see below)
- configure real_meter (see below)
- configure fake_meter (see below)


## real_meter.py

- Make sure your user has permissions on the serial towards the meter. To add your user to the dialout group `sudo usermod -G dialout -a [username]` and then logout/login.
- Copy real_meter.yaml.example to real_meter.yaml and adjust to your setup.
- Start it with `python3 real_meter.py`, if it starts to print out values go ahead and make it start automatically.
- Either use the provided systemd file or use /etc/rc.local

Put the following into /etc/rc.local (if this is preferred), substitute "pelle" with the user that should run the script and modify the path if you have the code somewhere else:

```
sudo -u pelle tmux new-session -d -s real_meter 'cd /home/pelle/smartmeter/code && ./start-meter.sh'
```

"start-meter.sh" will restart the script if it fails, this will enable things like backup power etc.


## fake_meter.py

- Copy fake_meter.yaml.example to fake_meter.yaml and adjust to your setup. If you provide a tcp meter, put the IP of your rpi together with a port. If the script runs as a non privileged user, use something like 1502 as the port. And then use iptables to translate incoming 502 to 1502.
- Start it with `python3 fake_meter.py`, if it starts to print out values go ahead and make it start automatically.
- Either use the provided systemd file or use /etc/rc.local

Put the following into /etc/rc.local (if this is preferred), substitute "pelle" with the user that should run the script and modify the path if you have the code somewhere else:

```
sudo -u pelle tmux new-session -d -s fake_meter 'cd /home/pelle/smartmeter/code && ./start-fake.sh'
```

If you need multiple fake meters on tcp, you can run multiple instances of the script on different ports. There is a bug in Fronius that doesn't allow you to change the modbus port (although you can change it in the GUI). To work around this, you can run iptables to redirect the port depending on which inverter that is communicating....
You can also have the inverters talking to the same port, which is the normal case unless you want to involve the power_controller (see below).

Example where fake_meter runs on 192.168.5.20, with one fake-script and two inverters:
```
in fake_meter.yaml:
inverter_interface: 192.168.5.20:1502
```

To redirect the normal modbus port 502 to 1502, add the following into /etc/rc.local (adjust addresses to your setup):
```
iptables -A PREROUTING -t nat -i eth0 -d 192.168.5.20 -p tcp --dport 502 -j REDIRECT --to-port 1502
```

## Inverter

- Configure the inverter to either use modbus rtu or tcp.
- If the "faker" is on tcp, configure the IP to the box where your "faker" runs, on port 512.

![Fronius smart meter](./fronius_meter.png "smart meter")


## power_controller.py

Optional, not needed!!! This is if you wish to script things on your own.
Configure according to your setup (inverter address, battery capacity etc). The script will need this to be able to disable power if the SoC is out of range etc.
As the script is a bit sensitive at the moment, start it with start-power.sh, this will enable automatic restart in case of critical error.


## MQTT broker (preferrably on RPi with real_meter)

Installation instructions:

```
sudo apt install mosquitto mosquitto-clients
sudo systemctl enable mosquitto
sudo mosquitto_passwd -c /etc/mosquitto/.passwd batt2gen24   # type "batt2gen24_pass" when asked for password
sudo nano /etc/mosquitto/conf.d/auth.conf
...content:
listener 1883
allow_anonymous false
password_file /etc/mosquitto/.passwd

sudo nano /etc/mosquitto/conf.d/websockets.conf 
...content:
listener 9001
protocol websockets

sudo systemctl restart mosquitto
```

## Normal output 

This is what the "real meter" normally looks like.
                                                   
```
2024-04-13 08:00:43Reading from Sungrow_dtsu666 Smart Meter  
[8405] 
[16128]                                                                                                                
[0, 397, 65535, 64969, 65535, 65400, 65535, 65230, 65535, 65512, 65535, 65264, 0, 27, 65535, 65268]
[4986]                          
[2345, 2351, 2359, 189, 298, 102]               
[27, 14497, 27, 14497, 0, 0, 0, 0, 0, 0, 2, 277]                                                                                                                                                                                               Smarthome Json: {"meter/data":{"e_consumed":17839.69,"e_produced":1313.49,"er_consumed":0,"er_produced":0,"f":49.86,"u1":234.5,"u2":235.1,"u3":235.9,"i1":1.89,"i2":-2.98,"i3":-1.02,"pa1":420,"pa2":837,"pa3":162,"pat":572,"pf1":0.9452,"pf2"
:0.6762,"pf3":0.8333,"pft":0.5332,"p1":397,"p2":-566,"p3":-135,"pt":-305,"pr1":-23,"pr2":-271,"pr3":27,"prt":-267,"last_updated":"1712988043"}}
Data published counter: 800                                                                                                                                                                                                                    Json: {"e_consumed":17839.69,"e_produced":1313.49,"er_consumed":0.0,"er_produced":0.0,"f":49.86,"u1":234.5,"u2":235.1,"u3":235.9,"i1":1.89,"i2":-2.98,"i3":-1.02,"pa1":420,"pa2":837,"pa3":162,"pat":572,"pf1":0.9452,"pf2":0.6762,"pf3":0.8333,"pft":0.5332,"p1":397.0,"p2":-566.0,"p3":-135.0,"pt":-305.0,"pr1":-23.0,"pr2":-271.0,"pr3":27.0,"prt":-267.0,"registers":{"r20480":[8405],"r63":[16128],"r356":[0,397,65535,64969,65535,65400,65535,65230,65535,65512,65535,65264,0,27,65535,6
5268],"r119":[4986],"r97":[2345,2351,2359,189,298,102],"r10":[27,14497,27,14497,0,0,0,0,0,0,2,277]},"last_updated":"1712988043"}
Data published counter: 800                
```


This is what the "faker" normally looks like.
                                                   
```
-------------------------                                                                                                                                                                   
Type: Fronius_ts65a3_tcp 
p1: 110.0 p2: 368.0 p3: 55.0 ptot: 582.0
pr1: 67.0 pr2: -170.0 pr3: 126.0 prtot: 23.0
pa1: 177 pa2: 538 pa3: 181 patot: 605       
pf1: 0.6215 pf2: 0.684 pf3: 0.3039 pftot: 0.962
i1: 1.03 i2: 2.22 i3: 1.0                        
u1: 234.8 u2: 235.3 u3: 236.1
reg_258: [2354, 0, 4072, 0, 5820, 0, 6050, 0, 230, 0, 962, 0, 0, 0, 498, 0]                                            
reg_286: [4062, 0, 2348, 0, 1030, 0, 1100, 0, 1770, 0, 670, 0, 622, 0, 4070, 0, 2353, 0, 2220, 0, 3680, 0, 5380, 0, 63835, 65535, 684, 0, 4084, 0, 2361, 0, 1000, 0, 550, 0, 1810, 0, 1260, 0, 304, 0]                                         
reg_1024: [17839, 0, 680, 0, 0, 0, 0, 0, 1313, 0, 480, 0, 0, 0, 0, 0]                                                                                                                                                                          
 Received message b'{"e_consumed":17839.68,"e_produced":1313.48,"er_consumed":0.0,"er_produced":0.0,"f":49.86,"u1":234.9,"u2":235.4,"u3":236.3,"i1":0.95,"i2":1.85,"i3":-1.12,"pa1":89,"pa2":474,"pa3":165,"pat":301,"pf1":0.2697,"pf2":0.6414,"pf3":0.2606,"pft":0.9468,"p1":24.0,"p2":304.0,"p3":-43.0,"pt":285.0,"pr1":65.0,"pr2":-170.0,"pr3":122.0,"prt":16.0,"registers":{"r20480":[8405],"r63":[16128],"r356":[0,24,0,304,65535,65492,0,285,0,65,65535,65365,0,122,0,16],"r119":[4986],"r97":[2349,2354,2363,95,185,112],"r10":[27,14496,27,14496,0,0,0,0,0,0,2,276]},"last_updated":"1712987574"}' on topic 'meter/data' with QoS 0      
```
-------------------------                                                                                                                                                                   

The "Received message" above is what is sent from the real meter over MQTT.
- First letter indicates physical quantity (e for energy, p for power, u for voltage, i for current and f for frequency).
- If there is a second letter it could indicate total-t, apparent-a, real-r, f for factor.
- If the second character is a digit, it indicates the phase. 
- "last_updated" is the timestamp in seconds, which is used to indicate if the data is old - a possible outage
- The "registers" are mostly informational, for debugging purposes.


# Tests

This is work in progress. Some tests exist for checking coding/decoding of values.

To run test: `code $ python3 -m unittest tests/coding_test.py -v`

