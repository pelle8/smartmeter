import unittest

class TestStringMethods(unittest.TestCase):


#10.12 kW [35696,1] (1*65536 + 35696)/10 
#- 9.54kW [35696,65534] -(2*65536 - 35696)/10
#- 2.98kW [35696,65535] -(1*65536 - 35696)/10

    def test_bit_coding_real(self):
        target = __import__('real_meter')
        self.assertEqual(1000, target.fronius_decode_value_32bits([1000,0],0))
        self.assertEqual(-1000, target.fronius_decode_value_32bits([64535,65535],0))
        self.assertEqual(101232, target.fronius_decode_value_32bits([35696,1],0))
        self.assertEqual(166768, target.fronius_decode_value_32bits([35696,2],0))
        self.assertEqual(-29839, target.fronius_decode_value_32bits([35696,65535],0))
        self.assertEqual(-95375, target.fronius_decode_value_32bits([35696,65534],0))

        self.assertEqual(8.203, target.fronius_decode_value_64bits([8,0,203,0],0))
        self.assertEqual(65609.739, target.fronius_decode_value_64bits([8,1,203,1],0))

        self.assertEqual(131073, target.get_value_from_32bits(1,2))
        self.assertEqual(-65534, target.get_value_from_32bits(1,65535))


    # Fronius TS65A-3:
    def test_fronius_ts65a3(self):
        target = __import__('real_meter')
        r258 = [2340 ,0 ,4053 ,0 ,47739 ,65535 ,17799 ,0 ,254 ,0 ,1000 ,0 ,0 ,0 ,499 ,0]
        r286 = [4055 ,0 ,2344 ,0 ,63002 ,65535 ,59599 ,65535 ,5937 ,0 ,22 ,0 ,1000 ,0 ,4049 ,0 ,2339 ,0 ,62990 ,65535 ,59587 ,65535 ,5950 ,0 ,109 ,0 ,1000 ,0 ,4053 ,0 ,2337 ,0 ,63005 ,65535 ,59626 ,65535 ,5912 ,0 ,122 ,0 ,1000 ,0]
        r1024 = [2005 ,0 ,163 ,0 ,43 ,0 ,801 ,0 ,1722 ,0 ,232 ,0 ,8 ,0 ,203 ,0]
        ret = target.decode_values("Fronius_ts65a3",{ "r258":r258,"r286":r286,"r1024":r1024 })
        expected_values = {'e_consumed': 2005.163, 'e_produced': 1722.232, 'er_consumed': 43.801, 'er_produced': 8.203, 'f': 49.9, 'u1': 234.4, 'u2': 233.9, 'u3': 233.7, 'i1': -2.53, 'i2': -2.54, 'i3': -2.53, 'pa1': 593.7, 'pa2': 595.0, 'pa3': 591.2, 'pat': 1779.9, 'pf1': 1, 'pf2': 1, 'pf3': 1, 'pft': 1, 'p1': -593.6, 'p2': -594.8, 'p3': -590.9, 'pt': -1779.6, 'pr1': 2.2, 'pr2': 10.9, 'pr3': 12.2, 'prt': 25.4}
        #print("\nR:",ret)
        #print("E:",expected_values)
        self.assertEqual(expected_values, ret)
        # And backwards...
        target_fake = __import__('fake_meter')
        registers = target_fake.convert_values_to_register(expected_values,"Fronius_ts5ka3")
        #print("\nR:",registers['reg_286'])
        # Some rounding and mean values lead to small differences
        reg258 = [2340, 0, 4048, 0, 47739, 65535, 17799, 0, 254, 0, 1000, 0, 0, 0, 499, 0]
        reg286 = [4055, 0, 2344, 0, 2530, 0, 59599, 65535, 5937, 0, 22, 0, 1000, 0, 4046, 0, 2339, 0, 2540, 0, 59587, 65535, 5950, 0, 109, 0, 1000, 0, 4043, 0, 2337, 0, 2530, 0, 59626, 65535, 5912, 0, 122, 0, 1000, 0]
        reg1024 = [2005, 0, 163, 0, 43, 0, 801, 0, 1722, 0, 231, 0, 8, 0, 202, 0]
        self.assertEqual(reg258, registers['reg_258'])
        self.assertEqual(reg286 , registers['reg_286'])
        self.assertEqual(reg1024 , registers['reg_1024'])

    # Fronius TS5KA-3:
    def test_fronius_ts5ka3(self):
        target = __import__('real_meter')
        r258 = [2329,0  ,4033,0,  35696,1,  37448,1,  47461,65535,  983,0,0,0,499,0]
        r286 = [4013,0,2291,0,16550,0,39383,0,39737,0,60238,65535,988,0,4066,0,2367,0,13500,0,30220,0,31255,0,57560,65535,967,0,4020,0,2328,0,14050,0,31628,0,31991,0,60736,65535,989,0]
        r1024 = [3492,0,270,0,4545,0,41,0,3722,0,929,0,2968,0,395,0]
        ret = target.decode_values("Fronius_ts5ka3",{ "r258":r258,"r286":r286,"r1024":r1024 })
        expected_values =  {'e_consumed': 3492.27, 'e_produced': 3722.929, 'er_consumed': 4545.041, 'er_produced': 2968.395, 'f': 49.9, 'u1': 229.1, 'u2': 236.7, 'u3': 232.8, 'i1': 16.55, 'i2': 13.5, 'i3': 14.05, 'pa1': 3973.7, 'pa2': 3125.5, 'pa3': 3199.1, 'pat': 10298.4, 'pf1': 0.988, 'pf2': 0.967, 'pf3': 0.989, 'pft': 0.983, 'p1': 3938.3, 'p2': 3022.0, 'p3': 3162.8, 'pt': 10123.2, 'pr1': -529.7, 'pr2': -797.5, 'pr3': -479.9, 'prt': -1807.4}
        self.assertEqual(expected_values, ret)
        # And backwards...
        target_fake = __import__('fake_meter')
        registers = target_fake.convert_values_to_register(expected_values,"Fronius_ts5ka3")
        #print("\nR:",registers['reg_286'])
        # Some rounding and mean values lead to small differences
        reg258 = [2328, 0, 4028, 0, 35696, 1, 37448, 1, 47461, 65535, 983, 0, 0, 0, 499, 0]
        reg286 = [3963, 0, 2291, 0, 16550, 0, 39383, 0, 39737, 0, 60238, 65535, 988, 0, 4094, 0, 2367, 0, 13500, 0, 30220, 0, 31255, 0, 57560, 65535, 967, 0, 4027, 0, 2328, 0, 14050, 0, 31628, 0, 31991, 0, 60736, 65535, 989, 0]
        reg1024 = [3492, 0, 269, 0, 4545, 0, 41, 0, 3722, 0, 929, 0, 2968, 0, 394, 0]
        self.assertEqual(reg258, registers['reg_258'])
        self.assertEqual(reg286 , registers['reg_286'])
        self.assertEqual(reg1024 , registers['reg_1024'])

    # Sungrow DTSU666:
    def test_sungrow_dtsu666(self):
        target = __import__('real_meter')
        r20480 = [8405]
        r63 = [16128]
        r356 = [0,857,65535,65339,0,579,0,1239,65535,65492,65535,65384,0,90,65535,65430]
        r119 = [4994]
        r97 = [2313,2322,2317,377,193,258]
        r10 = [0,12045,0,12045,0,0,0,0,0,0,0,3210]
        ret = target.decode_values("Sungrow_dtsu666",{ "r20480":r20480,"r63":r63,"r356":r356,"r119":r119,"r97":r97,"r10":r10 })
        expected_values = {'e_consumed': 120.45, 'e_produced': 32.1, 'er_consumed': 0, 'er_produced': 0, 'f': 49.94, 'u1': 231.3, 'u2': 232.2, 'u3': 231.7, 'i1': 3.77, 'i2': -1.93, 'i3': 2.58, 'pa1': 900, 'pa2': 347, 'pa3': 669, 'pat': 1344, 'pf1': 0.9522, 'pf2': 0.5648, 'pf3': 0.8655, 'pft': 0.9219, 'p1': 857, 'p2': -196, 'p3': 579, 'pt': 1239, 'pr1': -43, 'pr2': -151, 'pr3': 90, 'prt': -105}
        self.assertEqual(expected_values, ret)
        # And backwards...
        target_fake = __import__('fake_meter')
        registers = target_fake.convert_values_to_register(expected_values,"Sungrow_dtsu666")
        self.assertEqual(r10, registers['reg_10'])
        self.assertEqual(r97, registers['reg_97'])
        self.assertEqual(r119, registers['reg_119'])
        self.assertEqual(r356, registers['reg_356'])


    def test_divide_inverters(self):
        target = __import__('real_meter')
        result_dict = {"e_consumed":14771.49,"e_produced":1206.41,"er_consumed":0,"er_produced":0,"f":49.88,"u1":230.8,"u2":231.3,"u3":232.1,"i1":9.37,"i2":10.7,"i3":9.23,"p1":2152,"p2":2423,"p3":2132,"pt":6708,"pr1":-62,"pr2":-263,"pr3":93,"prt":-233,"r20480":[8405],"r63":[16128],"r356":[0,2152,0,2423,0,2132,0,6708,65535,65473,65535,65272,0,93,65535,65302],"r119":[4988],"r97":[2308,2313,2321,937,1070,923],"r10":[22,35357,22,35357,0,0,0,0,0,0,1,55105],"last_updated":"1708704396"}
        ret = target.divide_by_inverters(result_dict,2)
        expected_values = {'e_consumed': 7385.745, 'e_produced': 603.205, 'er_consumed': 0.0, 'er_produced': 0.0, 'f': 49.88, 'u1': 230.8, 'u2': 231.3, 'u3': 232.1, 'i1': 4.685, 'i2': 5.35, 'i3': 4.615, 'p1': 1076.0, 'p2': 1211.5, 'p3': 1066.0, 'pt': 3354.0, 'pr1': -31.0, 'pr2': -131.5, 'pr3': 46.5, 'prt': -116.5, 'r20480': [8405], 'r63': [16128], 'r356': [0, 2152, 0, 2423, 0, 2132, 0, 6708, 65535, 65473, 65535, 65272, 0, 93, 65535, 65302], 'r119': [4988], 'r97': [2308, 2313, 2321, 937, 1070, 923], 'r10': [22, 35357, 22, 35357, 0, 0, 0, 0, 0, 0, 1, 55105], 'last_updated': '1708704396'}
        self.assertEqual(expected_values, ret)

    def test_bit_coding_fake(self):
        target = __import__('fake_meter')
        self.assertEqual([1000,0], target.get_32bits_from_value(1000))
        self.assertEqual([34464,1], target.get_32bits_from_value(100000))
        self.assertEqual([64535, 65535], target.get_32bits_from_value(-1000))
        self.assertEqual([31071, 65534], target.get_32bits_from_value(-100000))
        self.assertEqual([0, 1000], target.encode_value_to_32bits(1000))
        self.assertEqual([1, 34464], target.encode_value_to_32bits(100000))
        self.assertEqual([65535,64535], target.encode_value_to_32bits(-1000))
        self.assertEqual([65534,31071], target.encode_value_to_32bits(-100000))

    def test_compare_lists(self):
        target = __import__('fake_meter')
        debug = True
        self.assertEqual( True, target.compare_lists([10,100],[10,100],debug))
        self.assertEqual( True, target.compare_lists([0],[0],debug))
        self.assertEqual( True, target.compare_lists([],[],debug))
        self.assertEqual( False, target.compare_lists([10],[],debug))
        self.assertEqual( False, target.compare_lists([10],[1],debug))
        self.assertEqual( False, target.compare_lists([10,10],[10,11],debug))
        self.assertEqual( False, target.compare_lists([3965, 0, 2292, 0, 1092, 0, 1695, 0, 6855, 0, 248, 0, 990, 0, 4023, 0, 2326, 0, 825, 0, 64478, 65535, 6526, 0, 1242, 0, 64899, 65535, 3980, 0, 2301, 0, 1140, 0, 64898, 65535, 9734, 0, 2348, 0, 65274, 65535],[3970, 0, 2295, 0, 1085, 0, 1871, 0, 7516, 0, 170, 0, 989, 0, 4025, 0, 2327, 0, 837, 0, 64490, 65535, 6379, 0, 1204, 0, 64888, 65535, 3979, 0, 2300, 0, 1140, 0, 64894, 65535, 9459, 0, 2276, 0, 65274, 65535] ,debug))

    def test_compare_dicts(self):
        target = __import__('fake_meter')
        self.assertEqual( True, target.compare_dicts({},{}))
        self.assertEqual( True, target.compare_dicts({'a':1},{'a':1}))
        self.assertEqual( True, target.compare_dicts({'a':1, 'b':1},{'a':1, 'b':1}))
        self.assertEqual( False, target.compare_dicts({'a':1},{}))
        self.assertEqual( False, target.compare_dicts({'a':1},{'a':2}))
        self.assertEqual( False, target.compare_dicts({'a':1, 'b':1},{'a':1, 'b':2}))

    def test_check_modbus_data_values(self):
        target = __import__('fake_meter')
        self.assertEqual( 1, target.check_modbus_data_values([1,1],[1,1],0))
        self.assertEqual( 0, target.check_modbus_data_values([1,1],[1,2],0))




if __name__ == '__main__':
    unittest.main()
