## Fronuis Smart Meter TS65A-3 Registers 

Polled at startup, haven't reversed those yet

```
0, [2336,0]     AC Voltage mean?
11, [731]
770, [1,3)
20480, [50, 51, 48, 50, 53, 54, 87, 31973, 1450]    S/N?
20496, [2022]
4098, [0]
4355, [1]
4356, [1,0]
```


Polled constantly:

```
Read holding register for device ID 1 , Start address = 258 Quantity = 16
Read holding register for device ID 1 , Start address = 286 Quantity = 42
```

```
The values are encoded as 32-bit integers, with an odd coding of negative values (see python code below)

258 - 2340      234.0 AC Voltage Mean?
259 - 0
260 - 4053      405.3 AC Voltage Mean?
261 - 0 
262 - 47739     -1780.6W - Power total
263 - 65535
264 - 17799     1779.9 VA - Apparent Power total
265 - 0           
266 - 254       -2.54 VA - Reactive Power total
267 - 0
268 - 1000      Power factor
269 - 0
270 - 0
271 - 0
272 - 499       49.9 Hz - Frequency
273 - 0

286 - 4053      405.3 V - AC Voltage P1
287 - 0
288 - 2343      234.3 - AC Voltage L1
289 - 0
290 - 63001     -2.534A - AC Current L1
291 - 65535
292 - 59598     -593.7W - Power L1    
293 - 65535 
294 - 5938      593.3 - Apparent Power L1
295 - 0
296 - 22        2.2 VA - Reactive Power L1
297 - 0 
298 - 1000      Power factor
299 - 0 

300 - 4027      402.7 - AC Voltage P2
301 - 0
302 - 2338      233.8 - AC Voltage L2
303 - 0
304 - 62991     -2.544A - AC Current L2
305 - 65535
306 - 59589     -594.6W - Power L2
307 - 65535
308 - 5948      594.8 - Apparent Power L2
309 - 0
310 - 109       10.9 VA - Reactive Power L2
311 - 0
312 - 1000      Power factor = 1 (64537 = 0.999)
313 - 0

314 - 4072      407.2 - AC Voltage P3
315 - 0
316 - 2336      233.6 - AC Voltage L3
317 - 0
318 - 63005     -2.530A - AC Current L3 (65535-63005 mA)
319 - 65535
320 - 59626     -590.9W - Power L3 ( (65535-59626)/10 )
321 - 65535
322 - 5911      591.1 - Apparent Power L3
323 - 0
324 - 122       12.2 VA - Reactive Power L3
325 - 0
326 - 1000      Power factor
327 - 0

```


Polled every 10 seconds:

```
2023-03-20 17:19:30.073269 Read holding register for device ID 1 , Start address = 1024 Quantity = 16```
```

```
Values below were confirmed by looking at the actual meter display

1024 - 2005     Energy consumed 64bit (2005.163 kWh)
1025 - 0
1026 - 163       
1027 - 0
1028 - 43       Energy reactive consumed 64bit (43.801 kVArh)
1029 - 0
1030 - 801     
1031 - 0
1032 - 1722     Energy produced 64bit (1722.232 kWh)
1033 - 0
1034 - 232      
1035 - 0
1036 - 8        Energy reactive produced 64bit (8.203 kVArh)
1037 - 0      
1038 - 203
1039 - 0


```

## Python to code/decode values

Two consecutive 16-bit registers are used. If MSB in r2 is 1, then it's a negative number.

```
def get_value_from_32bits(r1,r2):

  v = r1 + (r2 & 32767) * 65536
  if r2 > 32768:
    v = - ((65535 - r1) + (65535 - r2) * 65536)
  print("v:",v)
  return v

def get_32bits_from_value(value):
  if value > 0:
    r1 = value & 65535
    r2 = value >> 16
  if value < 0:
    value = - value
    r1 = 65535 - (value & 65535)
    r2 = 65535 - (value >> 16)
  print("v,r1,r2:",value,r1,r2)
  print("")
  return [r1,r2]


v = get_value_from_32bits(64657,1) # 130193
new_regs = get_32bits_from_value(v // 1) 

v = get_value_from_32bits(17799,0) # 17799
new_regs = get_32bits_from_value(v // 1) 

v = get_value_from_32bits(47739,65535) # -17796
new_regs = get_32bits_from_value(v // 1) 

v = get_value_from_32bits(31071,65534) # -100000 
new_regs = get_32bits_from_value(-100000 // 1) 

```
