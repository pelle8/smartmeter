#!/usr/bin/env python
"""
Pymodbus Server With Updating Thread
- modified to spoof a Sungrow Smart Meter // Per Carlen
"""
# --------------------------------------------------------------------------- #
# import the modbus libraries we need
# --------------------------------------------------------------------------- #
from pymodbus.server import StartAsyncSerialServer
from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext
from pymodbus.transaction import ModbusRtuFramer, ModbusAsciiFramer
from pymodbus.datastore import ModbusSequentialDataBlock, ModbusSparseDataBlock

#from twisted.internet.task import LoopingCall
import time
import json
import yaml
import paho.mqtt.client as paho
import copy
import asyncio

import logging
#FORMAT = ('%(asctime)-15s %(threadName)-15s'
#          ' %(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
#logging.basicConfig(format=FORMAT)
#log = logging.getLogger()
#log.setLevel(logging.DEBUG)

# --------------------------------------------------------------------------- #
# some global vars
# --------------------------------------------------------------------------- #
meter_debug = 0

meter_timeout = 120
last_updated = 0

reg_20480 = []
reg_63 = []
reg_356 = []
reg_119 = []
reg_97 = []
reg_10 = []

def read_config_file():

    config_json = ""
    filename = "config.yaml"
    print("reading from:",filename)
    try:
      with open(filename,'r') as file:
        config_json = json.dumps(yaml.safe_load(file))
    except:
      print("ERROR: No config file - ",filename)
      exit(1)

    return config_json




# --------------------------------------------------------------------------- #
# configure logging
# --------------------------------------------------------------------------- #
import logging
if meter_debug:
    logging.basicConfig()
    log = logging.getLogger()
    log.setLevel(logging.DEBUG)

# --------------------------------------------------------------------------- #
# thread to update the registers from mqtt data
# --------------------------------------------------------------------------- #
async def check_registers(a):
    global reg_20480
    global reg_63
    global reg_356
    global reg_119
    global reg_97
    global reg_10
    global timeout
    global meter_debug
    global last_updated

    while 1:
      timestamp_now = int(time.time())
      context = a
      slave_id = 0
      # Read the current stored values in the registers
      values_20480 = context[slave_id].getValues(3, 20480, count=1)
      values_63 = context[slave_id].getValues(3, 63, count=1)
      values_356 = context[slave_id].getValues(3, 356, count=8)
      values_119 = context[slave_id].getValues(3, 119, count=1)
      values_97 = context[slave_id].getValues(3, 97, count=6)
      values_10 = context[slave_id].getValues(3, 10, count=12)
      print("before:",values_356)

      if values_20480 == reg_20480 and values_63 == reg_63 and values_356 == reg_356 and \
      values_119 == reg_119 and values_97 == reg_97 and values_10 == reg_10:
        if meter_debug: print("No need to update modbus registers")

      if (timestamp_now - int(last_updated) > int(meter_timeout)): 
        print("Timeout on new data from real meter, exiting")
        exit(1)

      context[slave_id].setValues(3,10, reg_10)
      context[slave_id].setValues(3,63, reg_63)
      context[slave_id].setValues(3,97, reg_97)
      context[slave_id].setValues(3,119, reg_119)
      context[slave_id].setValues(3,356, reg_356)
      context[slave_id].setValues(3,20480, reg_20480)
      await asyncio.sleep(0.5)

def init_datastore():
    global context
    # ----------------------------------------------------------------------- # 
    # initialize your data store
    # ----------------------------------------------------------------------- # 
    slave_id = 0

    p63 = [ 16128 ]
    p119 = [ 4999 ]
    p356 = [ 0,269,65535,65494,0,29,0,256 ]
    p20480 = [ 8405 ]
    p97 = [ 2300,2308,2308,180,180,97 ]
    p10 = [ 0,11915,0,11915,0,0,0,0,0,0,0,3209 ]

    block = ModbusSparseDataBlock({ 11:p10, 64: p63, 98: p97, 120: p119, 357: p356, 20481:p20480 })

    #block = ModbusSparseDataBlock({ 0: [0]*2, 11: [0], 770: [0]*2, 20480: [0]*9, 20496: [0], 4096: [0], 4355: [0], 4356: [0]*2, 258: [0]*17, 286: [0]*43, 1024: [0]*17, 768:[0]*2 })
    store = ModbusSlaveContext(di=block, co=block, hr=block, ir=block,unit=254)
    context = ModbusServerContext(slaves= store, single=True)
    context[slave_id].setValues(3,0, [2336,0])
    context[slave_id].setValues(3,11, [731])
    context[slave_id].setValues(3,770, [1,3])
    context[slave_id].setValues(3,20480, [50, 51, 48, 50, 53, 54, 87, 31973, 1450])
    context[slave_id].setValues(3,20496, [2022])
    context[slave_id].setValues(3,4098, [0])
    context[slave_id].setValues(3,4355, [1])
    context[slave_id].setValues(3,4356, [1,0])

    #values_258 = context[0].getValues(3, 258, count=16)
    return context


def mqtt_on_message(mqtt_client,userdata, message,tmp=None):
    global meter_debug
    global last_updated
    global reg_20480
    global reg_63
    global reg_356
    global reg_119
    global reg_97
    global reg_10

    if meter_debug:
      print(" Received message " + str(message.payload)
        + " on topic '" + message.topic
        + "' with QoS " + str(message.qos))


    if message.topic == "meter/data":
      if meter_debug: print("Got mqtt update")
      json_data = json.loads(message.payload)
      if type(json_data) is dict or type(json_data) is list:
        last_updated = json_data['last_updated']
        if 'r10' in json_data:
          reg_10 = json_data['r10']
        if 'r63' in json_data:
          reg_63 = json_data['r63']
        if 'r97' in json_data:
          reg_97 = json_data['r97']
        if 'r119' in json_data:
          reg_119 = json_data['r119']
        if 'r356' in json_data:
          reg_356 = json_data['r356']
        if 'r20480' in json_data:
          reg_20480 = json_data['r20480']

def mqtt_on_subscribe(client, userdata, mid, granted_qos):
    #print(" Received message " + str(client)
    #    + "' with QoS " + str(granted_qos))
    pass

def mqtt_on_connect(client, userdata, flags, rc):
    if rc != 0:
      print("MQTT connection problem")
      client.connected_flag=False
    else:
      print("MQTT client connected:" + str(client))
      client.connected_flag=True


def run_updating_server():

    global meter_data
    global mqtt_client1
    global mqtt_client2
    global reg_10
    global meter_debug

    print("\nStarting meter_handler")
    print("Reading config from file")
    config = json.loads(read_config_file())
    if config:
      meter_timeout = config['meter_timeout']
      meter_debug = config['meter_debug']
      inverter_interface = config['inverter_interface']
      mqtt_broker = config['mqtt_broker']
      mqtt_port = config['mqtt_port']
      mqtt_username = config['mqtt_username']
      mqtt_password = config['mqtt_password']
    else:
      exit(1)

    # this mqtt client will subscribe
    mqtt_client2 = paho.Client(client_id="meter_sub",transport="tcp",protocol=paho.MQTTv311,clean_session=True)  
    mqtt_client2.on_subscribe = mqtt_on_subscribe
    mqtt_client2.on_connect = mqtt_on_connect
    mqtt_client2.on_message = mqtt_on_message
    mqtt_client2.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client2.connect(mqtt_broker,mqtt_port,keepalive=60)
    mqtt_client2.subscribe("meter/data",2)
    mqtt_client2.loop_start()

    if meter_debug:
        print("init datastore")
    context = init_datastore()
    #tmp=(context,)
    # ----------------------------------------------------------------------- # 
    # initialize the server information
    # ----------------------------------------------------------------------- # 
    identity = ModbusDeviceIdentification()
    identity.VendorName = 'pymodbus'
    identity.ProductCode = 'PM'
    identity.VendorUrl = 'http://github.com/riptideio/pymodbus/'
    identity.ProductName = 'pymodbus Server'
    identity.ModelName = 'pymodbus Server'


    #time_loop_1 = 4  # 4 seconds delay
    #loop_1 = LoopingCall(f=update_registers, a=(context,)) # updates modbus registers from mqtt messages
    #loop_1.start(time_loop_1, now=False) # initially delay by time

    l_10 = len(reg_10)
    while l_10 < 12:
      if meter_debug: print("No mqtt data yet")
      time.sleep(1)
      l_10 = len(reg_10)
#    StartSerialServer(context, framer=ModbusRtuFramer, identity=identity,port=inverter_interface, timeout=.005, baudrate=9600)
#    exit(1)
    print("Starting modbus server")
    asyncio.run(run_async_server(context, identity, inverter_interface), debug=True)
    print("Server ended")
    exit(1)

async def run_async_server(context, identity, inverter_interface):
    print("Register task for register check")
    asyncio.create_task(check_registers(context))
    await StartAsyncSerialServer(context=context, framer=ModbusRtuFramer, identity=identity, port=inverter_interface, timeout=.005, baudrate=9600)

if __name__ == '__main__':
    run_updating_server()

