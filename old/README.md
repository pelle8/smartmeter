# Old stuff

## fronius_real_meter.py / sungrow_real_meter.py

This is the script that is connected to the real meter. 
It reads registers and publishes the received data on MQTT.

## fronius_fake_meter.py / sungrow_fake_meter.py 

This is the script that is connected to the inverter(s).
It subscribes on meter/data on MQTT and puts the data into registers, available for an inverter to read.

## Configuration

Adjust config.yaml to match your setup


# Controlling a Gen24 using data from a smart meter over IP using RPi(s)

## gen24_control_from_meter_sungrow.py

A script that reads values coming from a Sungrow smart meter over MQTT and then controls a Gen24 inverter over modbus tcp. Aims to have 0W at grid connection point.

## gen24_control_from_meter_tillquist.py

A script that reads values coming from an old Tillquist not so smart meter over RS485 and then controls a Gen24 inverter over modbus tcp. Aims to have 0W at grid connection point.
