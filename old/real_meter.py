import os

from pymodbus.client.sync import ModbusSerialClient

"""
Pymodbus Client 
- Reads data from Fronius Smart Meter and publishes with mqtt // Per Carlen
- add MultiInverter Support // Instragram: alex4skate
"""

import time
import json
import yaml
from datetime import datetime
import paho.mqtt.client as paho

meter_probe_interval = 2
inverter_count = 1
first_start = 1
smart_meter_mqtt = {}
ONOFF_SENSORS_MAP = {
    'power': {'eid': 'power', 'uom': 'W', 'icon': 'mdi:flash-outline'},
    'current': {'eid': 'current', 'uom': 'A', 'icon': 'mdi:current-ac'},
    'voltage': {'eid': 'voltage', 'uom': 'V', 'icon': 'mdi:power-plug'},
    'dusty': {'eid': 'dusty', 'uom': 'µg/m3', 'icon': 'mdi:select-inverse'},
    'light': {'eid': 'light', 'uom': 'lx', 'icon': 'mdi:car-parking-lights'},
    'noise': {'eid': 'noise', 'uom': 'Db', 'icon': 'mdi:surround-sound'},
    'currentHumidity': {'eid': 'humidity', 'uom': '%', 'icon': 'mdi:water-percent'},
    'humidity': {'eid': 'humidity', 'uom': '%', 'icon': 'mdi:water-percent'},
    'currentTemperature': {'eid': 'temperature', 'uom': 'C', 'icon': 'mdi:thermometer'},
    'temperature': {'eid': 'temperature', 'uom': 'C', 'icon': 'mdi:thermometer'}
}

def read_inverter_count(inverter_interface):
  global inverter_count
  global first_start
  if type(inverter_interface) == list:
    inverter_count = len(inverter_interface)

def read_config_file():
    config_json = ""
    filename = "config.yaml"
    path = "/etc/smartmeter/"
    if os.path.isfile(path + filename):
        filename = path + filename
    print("reading from:", filename)
    try:
        with open(filename, 'r') as file:
            config_json = json.dumps(yaml.safe_load(file))
    except:
        print("ERROR: No config file - ", filename)
        exit(1)

    return config_json



def print2log(msg):
  return msg


def get_rr(rr):
  ret = []
  for x in rr.registers:
    ret.append(x)
  return ret


def print_rr(rr,var_type):
  ret = ""
  for x in rr.registers:
    if var_type == "hex":
      s = x.to_bytes(2,"big").hex()
      ret += print2log(s + " ")
    if var_type == "dec":
      ret += print2log(str(x) + ",")
    if var_type == "str":
      s = x.to_bytes(2,"big")
      ret += print2log(str(chr(s[0])) + str(chr(s[1])) + ",")
  ret += print2log("")
  return ret.rstrip(',')


def read(reg,count,unit_id,var_type):
  global meter_debug

  ret = []
  #str = ""
  retry_counter = 1
  while retry_counter > 0:
    rr = client.read_holding_registers(reg,count , unit=unit_id)
    if not rr.isError():

      ret = get_rr(rr)
      retry_counter = 0
    if retry_counter > 0:
      time.sleep(0.5)
    retry_counter -= 1
  #if retry_counter == 0:
    #str += str(reg) + ", timeout, for unit_id: " + str(unit_id)
    #print(str)
  #else:
    #if meter_debug: print(str)

  return ret

def mqtt_on_publish(mqtt_client,userdata,result): 
    global meter_debug
    if meter_debug > 1: print("data published:", result)
    pass


def mqtt_on_connect(client, userdata, flags, rc):
    if rc != 0:
        if meter_debug > 1: print("MQTT connection problem")
        client.connected_flag = False
    else:
        if meter_debug > 1: print("MQTT client connected:" + str(client))
        client.connected_flag = True

def publish(client,msg_string,topic):
    ret = client.publish(topic,msg_string)


def debug_startregister(register, size):
  reg = read(register, size, addr, "dec")
  counter = register
  for register in reg:
    print("register: " + str(counter) + ", value: " + str(register))
    counter += 1

def read_from_fronius_smart_meter():
  global meter_debug
  global reg_258, reg_286, reg_1024, first_start, smart_meter_mqtt

  ret = False
  addr = 1
  if meter_debug: print(dt_string + " - Reading from Fronius Smart Meter ")
  reg_258 = read(258, 16, addr, "dec")
  reg_286 = read(286, 42, addr, "dec")
  reg_1024 = read(1024, 16, addr, "dec")
  if meter_debug and first_start:
      debug_startregister(0, 2)
      debug_startregister(11, 1)
      debug_startregister(770, 2)
      debug_startregister(20480, 9)
      debug_startregister(20496, 1)
      debug_startregister(4098, 1)
      debug_startregister(4355, 3)
      first_start = 0

  if len(reg_258) > 15 and len(reg_286) > 41 and len(reg_1024) > 15:
      smart_meter_mqtt = {'Time': timestamp,
                          'Current AC Phase 1': decode_value(reg_286, 4) / 10,
                          'Current AC Phase 2': decode_value(reg_286, 22) / 10,
                          'Current AC Phase 3': decode_value(reg_286, 36) / 10,
                          'Frequency_Phase_Average': decode_value(reg_258, 14) / 10,
                          'PowerApparent S Phase 1': decode_value(reg_286, 10) / 10,
                          'PowerApparent S Phase 2': decode_value(reg_286, 24) / 10,
                          'PowerApparent S Phase 3': decode_value(reg_286, 38) / 10,
                          'PowerApparent S Sum': decode_value(reg_258, 4) / 10,
                          'PowerReal P Phase 1': decode_value(reg_286, 6) / 10,
                          'PowerReal P Phase 2': decode_value(reg_286, 20) / 10,
                          'PowerReal P Phase 3': decode_value(reg_286, 34) / 10,
                          'Voltage AC Phase 1': decode_value(reg_286, 2) / 10,
                          'Voltage AC Phase 2': decode_value(reg_286, 16) / 10,
                          'Voltage AC Phase 3': decode_value(reg_286, 30) / 10,
                          'Voltage_AC_PhaseToPhase_12': decode_value(reg_286, 0) / 10,
                          'Voltage_AC_PhaseToPhase_23': decode_value(reg_286, 14) / 10,
                          'Voltage_AC_PhaseToPhase_31': decode_value(reg_286, 28) / 10,
                          'Model': 'Fronius Fake Smartmeter'}

      # recalculate the correct powervalues for the inverters divid thru the inverter count
      if meter_debug: print("Watts total register: " + str(reg_258[4]) + "," + str(reg_258[5]) + " decoded: " + str(
          decode_value(reg_258, 4) / 100))
      if meter_debug: print("Watts L1    register: " + str(reg_286[6]) + "," + str(reg_286[7]) + " decoded: " + str(
          decode_value(reg_286, 4) / 100))
      if meter_debug: print("Watts L2    register: " + str(reg_286[20]) + "," + str(reg_286[21]) + " decoded: " + str(
          decode_value(reg_286, 20) / 100))
      if meter_debug: print("Watts L3    register: " + str(reg_286[34]) + "," + str(reg_286[35]) + " decoded: " + str(
          decode_value(reg_286, 34) / 100))
      if inverter_count > 1:
          # total current values
          recalculate_new_value(reg_258, 4, inverter_count)  # Watt Power Total Current
          if meter_debug: print(
              "Modified Watts total register: " + str(reg_258[4]) + "," + str(reg_258[5]) + " decoded: " + str(
                  decode_value(reg_258, 4) / 100))
          recalculate_new_value(reg_258, 6, inverter_count)  # Apparent Power Total Current
          recalculate_new_value(reg_258, 8, inverter_count)  # VA Rective Power Total Current

          # phase 1 current values
      recalculate_new_value(reg_286, 4, inverter_count)  # A AC Current
      recalculate_new_value(reg_286, 6, inverter_count)  # W Power
      if meter_debug: print(
          "Modified Watts L1    register: " + str(reg_286[6]) + "," + str(reg_286[7]) + " decoded: " + str(
              decode_value(reg_286, 4) / 100))
      recalculate_new_value(reg_286, 8, inverter_count)  # Apparent Power
      recalculate_new_value(reg_286, 10, inverter_count)  # VA Reactive Power

      # phase 2 current values
      recalculate_new_value(reg_286, 18, inverter_count)  # A AC Current
      recalculate_new_value(reg_286, 20, inverter_count)  # W Power
      if meter_debug: print(
          "Modified Watts L2    register: " + str(reg_286[20]) + "," + str(reg_286[21]) + " decoded: " + str(
              decode_value(reg_286, 20) / 100))
      recalculate_new_value(reg_286, 22, inverter_count)  # Apparent Power
      recalculate_new_value(reg_286, 24, inverter_count)  # VA Reactive Power

      # phase 3 current values
      recalculate_new_value(reg_286,32, inverter_count) # A AC Current
      recalculate_new_value(reg_286,34, inverter_count) # W Power
      if meter_debug: print(
          "Modified Watts L3    register: " + str(reg_286[34]) + "," + str(reg_286[35]) + " decoded: " + str(
              decode_value(reg_286, 34) / 100))
      recalculate_new_value(reg_286,36,inverter_count) # Apparent Power
      recalculate_new_value(reg_286,38, inverter_count) # VA Reactive Power

  ret = True
  return ret

def recalculate_new_value(register_array, offset, divident):
    set_value(register_array, offset, decode_value(register_array,offset)//divident)
def set_value(register_array, offset, value):
 if value > 0:
    register_array[offset] = value & 65535
    register_array[offset+1] = (value >> 16)
 if value < 0:
    value = - value
    register_array[offset] = 65535 - (value & 65535)
    register_array[offset+1] = 65535 - (value >> 16)

def decode_value(register_array, offset):
  v = register_array[offset] + (register_array[offset+1] & 32767) * 65536
  if register_array[offset+1] > 32768:
    v = - ((65535 - register_array[offset]) + (65535 - register_array[offset+1]) * 65536)
  return v

def read_startup_from_fronius_smart_meter():

  global meter_debug
  global timestamp

  timestamp = str(int(time.time()))

  addr = 1
  if meter_debug: print (get_timestamp() + " - Reading from Fronius Smart Meter ")
  reg = read(0,2,addr,"dec")
  print("reg0:",reg)
  reg = read(11,1,addr,"dec")
  print("reg11:",reg)
  reg = read(770,1,addr,"dec")
  print("reg770:",reg)
  reg = read(770,2,addr,"dec")
  print("reg770:",reg)
  reg = read(20480,9,addr,"dec")
  print("reg20480:",reg)
  reg = read(20496,1,addr,"dec")
  print("reg20496:",reg)
  reg = read(4098,1,addr,"dec")
  print("reg4096:",reg)
  reg = read(4355,1,addr,"dec")
  print("reg4355:",reg)
  reg = read(4356,2,addr,"dec")
  print("reg4356:",reg)

  return

def get_timestamp():
  now = datetime.now()
  return now.strftime("%Y-%m-%d %H:%M:%S")


print("\nStarting handler of real meter")
print("Reading config from file")
config = json.loads(read_config_file())
smarthome = False
if config:
    meter_probe_interval = config['meter_probe_interval']
    meter_debug = config['meter_debug']
    meter_interface = config['meter_interface']
    read_inverter_count(config['inverter_interface'])
    mqtt_broker = config['mqtt_broker']
    mqtt_port = config['mqtt_port']
    mqtt_username = config['mqtt_username']
    mqtt_password = config['mqtt_password']

    try:
        mqtt_smarthome_broker = config['mqtt_smarthome_broker']
        mqtt_smarthome_port = config['mqtt_smarthome_port']
        mqtt_smarthome_username = config['mqtt_smarthome_username']
        mqtt_smarthome_password = config['mqtt_smarthome_password']
        smarthome = True
    except KeyError:
        print("no Smarthome MQTT defined")

else:
    exit(1)

client = ModbusSerialClient(
    method='rtu',
    port=meter_interface,
    baudrate=9600,
    timeout=1,
    parity='N',
    stopbits=1,
    bytesize=8
)

# read_startup_from_fronius_smart_meter()

# this mqtt client will publish
mqtt_client1 = paho.Client(client_id="meter_pub", transport="tcp", protocol=paho.MQTTv311, clean_session=True)
mqtt_client1.on_publish = mqtt_on_publish
mqtt_client1.on_connect = mqtt_on_connect
mqtt_client1.username_pw_set(mqtt_username, mqtt_password)
mqtt_client1.connect(mqtt_broker, mqtt_port, keepalive=60)
mqtt_client1.loop_start()

mqtt_client_smarthome = ""
if smarthome:
    mqtt_client_smarthome = paho.Client(client_id="fronius_real-fake_meter", transport="tcp", protocol=paho.MQTTv311,
                                        clean_session=True)
    mqtt_client_smarthome.on_publish = mqtt_on_publish
    mqtt_client_smarthome.on_connect = mqtt_on_connect
    mqtt_client_smarthome.username_pw_set(mqtt_smarthome_username, mqtt_smarthome_password)
    mqtt_client_smarthome.connect(mqtt_smarthome_broker, mqtt_smarthome_port, keepalive=60)
    mqtt_client_smarthome.loop_start()

addr = 1

if client.connect():
    try:
        print("client connected")
        while True:
            timestamp = str(int(time.time()))
            now = datetime.now()
            dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
            result = read_from_fronius_smart_meter()
            if result:
                json_string = json.dumps(
                    {'last_updated': timestamp, 'r258': reg_258, 'r286': reg_286, 'r1024': reg_1024},
                    separators=(',', ':'))
                if meter_debug > 1: print("Json:", json_string)
                publish(mqtt_client1, json_string, "meter/data")
                if smarthome:
                    json_string = json.dumps(
                        smart_meter_mqtt,
                        separators=(',', ':'))
                    if meter_debug > 1: print("Json:", json_string)
                    mqtt_client_smarthome.publish('tele/fronius_real_meter/STATE', json_string)

            else:
                if meter_debug: print("Error reading from meter")
        time.sleep(meter_probe_interval)
    except KeyboardInterrupt:
        pass
client.close()
