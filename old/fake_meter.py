#!/usr/bin/env python
"""
Pymodbus Server With Updating Thread
- modified to spoof a Fronius Smart Meter // Per Carlen
- add MultiInverter Support // Instragram: alex4skate
"""
import json
import logging
import os
import random
import sys
import time

import paho.mqtt.client as paho
import yaml
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext
from pymodbus.datastore import ModbusSparseDataBlock
from pymodbus.device import ModbusDeviceIdentification
from pymodbus.server.asynchronous import StartSerialServer
from pymodbus.transaction import ModbusRtuFramer
# --------------------------------------------------------------------------- #
# import the modbus libraries we need
# --------------------------------------------------------------------------- #
# from pymodbus.version import version
from twisted.internet.task import LoopingCall

FORMAT = ('%(asctime)-15s %(threadName)-15s'
          ' %(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.DEBUG)
inverter_interface = ""
# --------------------------------------------------------------------------- #
# some global vars
# --------------------------------------------------------------------------- #
meter_debug = 0

meter_timeout = 120
last_updated = 0

reg_258 = []
reg_286 = []
reg_1024 = []


def read_config_file():
    config_json = ""
    filename = "config.yaml"
    path = "/etc/smartmeter/"
    if os.path.isfile(path + filename):
        filename = path + filename
    print("reading from:", filename)
    try:
        with open(filename, 'r') as file:
            config_json = json.dumps(yaml.safe_load(file))
    except:
        print(inverter_interface + ": ERROR: No config file - ", filename)
        exit(1)

    return config_json




# --------------------------------------------------------------------------- #
# configure logging
# --------------------------------------------------------------------------- #
import logging

if meter_debug:
    logging.basicConfig()
    log = logging.getLogger()
    log.setLevel(logging.DEBUG)

# --------------------------------------------------------------------------- #
# thread to update the registers from mqtt data
# --------------------------------------------------------------------------- #
def update_registers(a):
    global reg_258
    global reg_286
    global reg_1024
    global timeout
    global meter_debug
    global last_updated

    timestamp_now = int(time.time())
    context = a[0]
    slave_id = 0x00

    # Read the current stored values in the registers
    try:
        values_258 = context[slave_id].getValues(3, 258, count=16)
        values_286 = context[slave_id].getValues(4, 286, count=42)
        values_1024 = context[slave_id].getValues(4, 1024, count=16)
        if meter_debug > 1: print("before:")
        if meter_debug > 1: print(values_258)
        if meter_debug > 1: print(values_286)
        if meter_debug > 1: print(values_1024)

        l_258 = len(reg_258)
        if l_258 < 16:
            return

        if values_258 == reg_258 and values_286 == reg_286 and values_1024 == reg_1024:
            if meter_debug: print(inverter_interface + ": No need to update modbus registers")
            return

        if (timestamp_now - int(last_updated) > int(meter_timeout)):
            print(inverter_interface + ": Timeout on new data from real meter, exiting")
            exit(1)

        context[slave_id].setValues(3, 258, reg_258)
        context[slave_id].setValues(3, 286, reg_286)
        context[slave_id].setValues(3, 1024, reg_1024)
        return
    except IndexError:
        if meter_debug > 1: print("no register received")

def init_datastore():
    # ----------------------------------------------------------------------- #
    # initialize your data store
    # ----------------------------------------------------------------------- #
    slave_id = 0
    block = ModbusSparseDataBlock(
        {0: [0] * 2, 11: [0], 770: [0] * 2, 20480: [0] * 9, 20496: [0], 4096: [0], 4355: [0], 4356: [0] * 2,
         258: [0] * 17, 286: [0] * 43, 1024: [0] * 17, 768: [0] * 2})
    store = ModbusSlaveContext(di=block, co=block, hr=block, ir=block, unit=1)
    context = ModbusServerContext(slaves=store, single=True)
    context[slave_id].setValues(3, 11, [731])
    if bool(random.getrandbits(1)):  # random smartmeter default values
        print("simulate pelle smartmeter")
        context[slave_id].setValues(3, 20480, [50, 51, 48, 50, 53, 54, 87, 31972, 1450])
        context[slave_id].setValues(3, 770, [1, 3])
        context[slave_id].setValues(3, 0, [2336, 0])
    else:
        print("simulate alex smartmeter")
        context[slave_id].setValues(3, 20480, [51, 49, 51, 51, 53, 53, 85, 26554, 488])
        context[slave_id].setValues(3, 770, [1, 10])
        context[slave_id].setValues(3, 0, [2264, 0])
    context[slave_id].setValues(3, 20496, [2022])
    context[slave_id].setValues(3, 4098, [0])
    context[slave_id].setValues(3, 4355, [1])
    context[slave_id].setValues(3, 4356, [1, 0])

    # values_258 = context[0].getValues(3, 258, count=16)
    return context


def mqtt_on_message(mqtt_client,userdata, message,tmp=None):
    global meter_debug
    global last_updated
    global reg_258
    global reg_286
    global reg_1024
    global inverter_interface

    if meter_debug > 1:
        print(inverter_interface + ": Received message " + str(message.payload)
              + " on topic '" + message.topic
              + "' with QoS " + str(message.qos))


    if message.topic == "meter/data":
        if meter_debug > 1: print(inverter_interface + ": Got mqtt update")
        json_data = json.loads(message.payload)
        if type(json_data) is dict or type(json_data) is list:
            last_updated = json_data['last_updated']
            if 'r258' in json_data:
                reg_258 = json_data['r258']
            if 'r286' in json_data:
                reg_286 = json_data['r286']
            if 'r1024' in json_data:
                reg_1024 = json_data['r1024']

def mqtt_on_subscribe(client, userdata, mid, granted_qos):
    #print(" Received message " + str(client)
    #    + "' with QoS " + str(granted_qos))
    pass

def mqtt_on_connect(client, userdata, flags, rc):
    global inverter_interface
    if rc != 0:
      print(inverter_interface+": MQTT connection problem")
      client.connected_flag=False
    else:
      print(inverter_interface+": MQTT client connected:" + str(client))
      client.connected_flag=True


def run_updating_server():

    global meter_data
    global mqtt_client1
    global mqtt_client2
    global reg_258
    global meter_debug
    global inverter_interface

    try:
        inverter_interface = sys.argv[1]
    except IndexError:
        print("Please use the port of modbus as parameter. /dev/ttyxxxx")
        exit(0)
    print(inverter_interface + ": Starting meter_handler")
    print(inverter_interface + ": Reading config from file")
    config = json.loads(read_config_file())
    if config:
        meter_timeout = config['meter_timeout']
        meter_debug = config['meter_debug']
        mqtt_broker = config['mqtt_broker']
        mqtt_port = config['mqtt_port']
        mqtt_username = config['mqtt_username']
        mqtt_password = config['mqtt_password']
    else:
      exit(1)

    # this mqtt client will subscribe
    mqtt_client2 = paho.Client(client_id="meter_sub"+inverter_interface,transport="tcp",protocol=paho.MQTTv311,clean_session=True)
    mqtt_client2.on_subscribe = mqtt_on_subscribe
    mqtt_client2.on_connect = mqtt_on_connect
    mqtt_client2.on_message = mqtt_on_message
    mqtt_client2.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client2.connect(mqtt_broker,mqtt_port,keepalive=60)
    mqtt_client2.subscribe("meter/data",2)
    mqtt_client2.loop_start()

    if meter_debug:
        print(inverter_interface+": init datastore")
    context = init_datastore()
    tmp=(context,)
    # ----------------------------------------------------------------------- #
    # initialize the server information
    # ----------------------------------------------------------------------- #
    identity = ModbusDeviceIdentification()
    identity.VendorName = 'pymodbus'
    identity.ProductCode = 'PM'
    identity.VendorUrl = 'http://github.com/riptideio/pymodbus/'
    identity.ProductName = 'pymodbus Server'
    identity.ModelName = 'pymodbus Server'
    #identity.MajorMinorRevision = version.short()

    time_loop_1 = 0.2  # 4 seconds delay
    loop_1 = LoopingCall(f=update_registers, a=(context,))  # updates modbus registers from mqtt messages
    loop_1.start(time_loop_1, now=True)  # initially delay by time

    l_258 = len(reg_258)
    while l_258 < 16:
      if meter_debug: print(inverter_interface+": No mqtt data yet")
      print(reg_258)
      time.sleep(1)
      l_258 = len(reg_258)
    StartSerialServer(context, framer=ModbusRtuFramer, identity=identity,port=inverter_interface, timeout=.005, baudrate=9600)
    exit(1)

if __name__ == '__main__':
    run_updating_server()
