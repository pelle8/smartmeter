from pymodbus.client.sync import ModbusSerialClient

"""
Pymodbus Client 
- Reads data from Sungrow Smart Meter and publishes with mqtt // Per Carlen
"""

import struct
import time
import requests
import os
import time
import json
import yaml
from datetime import datetime
import paho.mqtt.client as paho

#meter_probe_interval = 2


def read_config_file():

    config_json = ""
    filename = "config.yaml"
    print("reading from:",filename)
    try:
      with open(filename,'r') as file:
        config_json = json.dumps(yaml.safe_load(file))
    except:
      print("ERROR: No config file - ",filename)
      exit(1)

    return config_json



def print2log(msg):
  return msg


def get_rr(rr):
  ret = []
  for x in rr.registers:
    ret.append(x)
  return ret


def print_rr(rr,var_type):
  ret = ""
  for x in rr.registers:
    if var_type == "hex":
      s = x.to_bytes(2,"big").hex()
      ret += print2log(s + " ")
    if var_type == "dec":
      ret += print2log(str(x) + ",")
    if var_type == "str":
      s = x.to_bytes(2,"big")
      ret += print2log(str(chr(s[0])) + str(chr(s[1])) + ",")
  ret += print2log("")
  return ret.rstrip(',')


def read(reg,count,unit_id,var_type):
  global meter_debug

  ret = []
  #str = ""
  retry_counter = 1
  while retry_counter > 0:
    rr = client.read_holding_registers(reg,count , unit=unit_id)
    if not rr.isError():
      print(rr.registers)
      #str += print_rr(rr,"dec")
      ret = get_rr(rr)
      retry_counter = 0
    if retry_counter > 0:
      time.sleep(0.5)
    retry_counter -= 1
  #if retry_counter == 0:
    #str += str(reg) + ", timeout, for unit_id: " + str(unit_id)
    #print(str)
  #else:
    #if meter_debug: print(str)

  return ret

def mqtt_on_publish(mqtt_client,userdata,result): 
    global meter_debug
    if meter_debug: print("data published:",result)
    pass


def mqtt_on_connect(client, userdata, flags, rc):
    if rc != 0:
      print("MQTT connection problem")
      client.connected_flag=False
    else:
      print("MQTT client connected:" + str(client))
      client.connected_flag=True

def publish(client,msg_string,topic):
    #print("Publishing in",topic,msg_string)
    ret = client.publish(topic,msg_string) 

def read_from_smart_meter():

  global meter_debug
  global reg_20480
  global reg_63
  global reg_356
  global reg_119
  global reg_97
  global reg_10

  ret = False
  addr = 254
  if meter_debug: print (dt_string + " - Reading from Smart Meter ")
  reg_20480 = read(20480,1,addr,"dec")
  reg_63 = read(63,1,addr,"dec")
  reg_356 = read(356,8,addr,"dec")
  reg_119 = read(119,1,addr,"dec")
  reg_97 = read(97,6,addr,"dec")
  reg_10 = read(10,12,addr,"dec")
#  if len(reg_258) > 15 and len(reg_286) > 41 and len(reg_1024) > 15:
    # Remove us later, test to change some values
#    value = reg_258[0]
#    value += 1
#    reg_258[0] = value

  ret = True
  return ret


print("\nStarting handler of real meter")
print("Reading config from file")
config = json.loads(read_config_file())
if config:
  meter_probe_interval = config['meter_probe_interval']
  meter_debug = config['meter_debug']
  meter_interface = config['meter_interface']
  mqtt_broker = config['mqtt_broker']
  mqtt_port = config['mqtt_port']
  mqtt_username = config['mqtt_username']
  mqtt_password = config['mqtt_password']
else:
  exit(1)


print("Interface:",meter_interface)
client = ModbusSerialClient(
    method='rtu',
    port=meter_interface,
    baudrate=9600,
    timeout=1,
    parity='N',
    stopbits=1,
    bytesize=8
)



# this mqtt client will publish
mqtt_client1 = paho.Client(client_id="meter_pub",transport="tcp",protocol=paho.MQTTv311,clean_session=True)  
mqtt_client1.on_publish = mqtt_on_publish
mqtt_client1.on_connect = mqtt_on_connect
mqtt_client1.username_pw_set(mqtt_username, mqtt_password)
mqtt_client1.connect(mqtt_broker,mqtt_port,keepalive=60)
mqtt_client1.loop_start()

addr = 254

if client.connect():
  try:
    while True:
      timestamp = str(int(time.time()))
      now = datetime.now()
      dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
      result = read_from_smart_meter()
      if result:
        json_string = json.dumps({'last_updated' : timestamp, 'r20480': reg_20480, 'r63': reg_63, 'r356' : reg_356, 'r119' : reg_119, 'r97' : reg_97, 'r10' : reg_10}, separators=(',', ':'))
        if meter_debug: print("Json:",json_string)
        rc = publish(mqtt_client1,json_string,"meter/data")
      else:
        if meter_debug: print("Error reading from meter") 
      time.sleep(meter_probe_interval)
  except KeyboardInterrupt:
    pass
client.close()
