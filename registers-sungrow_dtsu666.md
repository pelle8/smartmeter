## Sungrow DTSU666 Smart Meter Registers

```
Static:
20480 - 8405
63 - 16128

The values are encoded as 32-bit integers, with an odd coding of negative values (see python code below)

Dynamic:

356:
[65535,65203 ,0,786 ,65535,65183 ,0,100, 65535,65475,65535,65397,0,90,65535,65426]

Example:
      65535,64501   -1035W  Power L1
      65535,64787   -749W   Power L2
      65535,64271   -1264W  Power L3
      65535,62485   -3050W  Power Tot
      x,y                   Reactive Power L1
      x,y                   Reactive Power L2
      x,y                   Reactive Power L3
      x,y                   Reactive Power Tot


119:  4994          49.94 Hz Grid freq

97:
[2330,2328,2334,165,376,166]

Example:
      2316          231.6V  Voltage L1   
      2325          232.5V  Voltage L2
      2328          232.8V  Voltage L3
      460           4.60A   Current L1
      366           3.66A   Current L2
      549           5.49A   Current L3

10:
[22,49991,22,49991,0,0,0,0,0,0,1,55142]

Example:
      0,12045       12045kWh  Consumed Energy
      0,12045
      0,0
      0,0
      0,0
      0,3210        3210kWh  Produced Energy

```

## Python to code/decode values

Two consecutive 16-bit registers are used. If MSB in r2 is 1, then it's a negative number.

```
def get_value_from_32bits(r1,r2):

  v = r1 + (r2 & 32767) * 65536
  if r2 > 32768:
    v = - ((65535 - r1) + (65535 - r2) * 65536)
  print("v:",v)
  return v

def get_32bits_from_value(value):
  if value > 0:
    r1 = value & 65535
    r2 = value >> 16
  if value < 0:
    value = - value
    r1 = 65535 - (value & 65535)
    r2 = 65535 - (value >> 16)
  print("v,r1,r2:",value,r1,r2)
  print("")
  return [r1,r2]


v = get_value_from_32bits(64657,1) # 130193
new_regs = get_32bits_from_value(v // 1) 

v = get_value_from_32bits(17799,0) # 17799
new_regs = get_32bits_from_value(v // 1) 

v = get_value_from_32bits(47739,65535) # -17796
new_regs = get_32bits_from_value(v // 1) 

v = get_value_from_32bits(31071,65534) # -100000 
new_regs = get_32bits_from_value(-100000 // 1) 

```
